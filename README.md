# NS-Modbus

This codebase is directly taken from the appendix of Mohammad Reza Sahraei's thesis. I was unable to find a public implementation of of it. Let me know if you know of one, and I'll redirect to it. 

This repository contains a slightly modified version to work with the most recent release of NS-3. The original code was designed to work with ns-3.13.

# Usage

Get the latest version of network simulator and this module.

	git clone https://gitlab.com/nsnam/ns-3-dev ns-3
	git clone https://gitlab.com/PMaynard/ns-modbus ns-3/contrib/ns-modbus

Configure and compile both. 

	./waf configure --enable-examples 
	./waf 

Run the examples.

	./waf --run ns-modbus-first-udp
	./waf --run ns-modbus-first-tcp 

# Origins

Ns-Modbus: Integration of Modbus with ns-3 Network Simulator by Mohammad Reza Sahraei, 2013, Simon Fraser University. [PDF](http://summit.sfu.ca/item/12972)