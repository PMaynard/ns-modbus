/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/netanim-module.h"
#include "ns3/internet-module.h"
#include "ns3/csma-helper.h"
#include "ns3/applications-module.h"
#include "ns3/modbus-udp-helper.h"
#include "ns3/modbus-mgr.h"
#include "sys/time.h"
// Network Topology (default)
//
// n0 n1 n2 n3
// |  |  |  |
// ================
// LAN 10.1.1.0
//
// How to Run
//
// set NS_LOG if required
// export NS_LOG=BusModbusUdp=level_all
//
// Example 1: Run bus topology MODBUS UDP with verbose logging:
// ./waf --run "scratch/busModbusUdp --verbose=1"
//
// Example 2: Run bus topology MODBUS UDP with 10 MODBUS clients:
// ./waf --run "scratch/busModbusUdp --verbose=0 --nCsma=10"
//
// MODBUS requests
uint8_t ENCAP_INTERFACE_TRANS_CANOPEN_GENERAL_REF_REQ_RSP_PDU[] = {0x2B, 0x0D};
uint8_t ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__BASIC_DEVICE_ID_OBJ_ID_00[] = {0x2B, 0x0E, 0x01, 0x00};
uint8_t ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__REGULAR_DEVICE_ID_OBJ_ID_00[] = {0x2B, 0x0E, 0x02, 0x00};
uint8_t ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__EXTENDED_DEVICE_ID_OBJ_ID_00[] = {0x2B, 0x0E, 0x03, 0x00};
uint8_t ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__SPECIFIC_DEVICE_ID_OBJ_ID_00[] = {0x2B, 0x0E, 0x04, 0x00};

using namespace ns3;
NS_LOG_COMPONENT_DEFINE ("BusModbusUdp");

int
main (int argc, char *argv[])
{
	struct timeval detail_time1, detail_time2, detail_time3;
// to get current time
	gettimeofday(&detail_time1, NULL);
	double t1 = detail_time1.tv_sec + (detail_time1.tv_usec/1000000.0);
	bool verbose = false;
	// std::string animFile = "bus-modbus-udp-animfile.xml";
//
// Default number of nodes. Overridable by command line argument.
//
	uint32_t nCsma = 4;
// under ns-3-dev type ./waf --run 'scratch/myfirst --PrintHelp'
// to see a list of parameters can be passed to the parser
	CommandLine cmd;
	cmd.AddValue ("nCsma", "Number of CSMA nodes/devices", nCsma);
	cmd.AddValue ("verbose", "Tell MODBUS applications to log if true", verbose);
	// cmd.AddValue ("animFile", "File Name for Animation Output", animFile);
	cmd.Parse (argc, argv);
	if (verbose)
	{
		LogComponentEnable ("ModbusUdpServer", LOG_LEVEL_INFO);
		LogComponentEnable ("ModbusUdpClient", LOG_LEVEL_INFO);
	}
	nCsma = nCsma == 0 ? 1 : nCsma;
	NS_LOG_INFO ("Build bus topology.");
	NodeContainer csmaNodes;
	csmaNodes.Create (nCsma);
	CsmaHelper csma;
	csma.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
	csma.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));
	NetDeviceContainer csmaDevices;
	csmaDevices = csma.Install (csmaNodes);
	NS_LOG_INFO ("Install internet stack on all nodes.");
	InternetStackHelper internet;
	internet.Install (csmaNodes);
	NS_LOG_INFO ("Assign IP Addresses.");
	Ipv4AddressHelper address;
	address.SetBase ("10.1.1.0", "255.255.255.0");
	Ipv4InterfaceContainer csmaInterfaces;
	csmaInterfaces = address.Assign (csmaDevices);
	NS_LOG_INFO ("Create applications.");
//
// Create a MODBUS UDP server on the star "hub"
//
	ModbusUdpServerHelper cModbusUdpServerHelper;
	ApplicationContainer serverApps = cModbusUdpServerHelper.Install (csmaNodes.Get (0));
	serverApps.Start (Seconds (1.0));
	serverApps.Stop (Seconds (10.0));
//
// Create MODBUS UDP client applications send UDP to the hub
//
	for (uint32_t i = 1; i < nCsma; ++i)
	{
		ModbusUdpClientHelper cModbusUdpClientHelper (csmaInterfaces.GetAddress (0));
		cModbusUdpClientHelper.SetAttribute ("MaxPackets", UintegerValue (1));
		cModbusUdpClientHelper.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
		ApplicationContainer clientApps = cModbusUdpClientHelper.Install (csmaNodes.Get (i));
// set MODBUS requests
		cModbusUdpClientHelper.SetRequest (clientApps.Get(0), Seconds (0.1),
		                                   ENCAP_INTERFACE_TRANS_CANOPEN_GENERAL_REF_REQ_RSP_PDU);
		cModbusUdpClientHelper.SetRequest (clientApps.Get(0), Seconds (0.2),
		                                   ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__BASIC_DEVICE_ID_OBJ_ID_00);
		cModbusUdpClientHelper.SetRequest (clientApps.Get(0), Seconds (0.1),
		                                   ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__SPECIFIC_DEVICE_ID_OBJ_ID_00);
		cModbusUdpClientHelper.SetCount (clientApps.Get(0), 2);
		clientApps.Start (Seconds (2.0));
		clientApps.Stop (Seconds (10.0));
	}
	NS_LOG_INFO ("Enable pcap tracing.");
//configure tracing
	AsciiTraceHelper ascii;
	csma.EnableAsciiAll (ascii.CreateFileStream ("bus-modbus-udp.tr"));
	csma.EnablePcapAll ("bus-modbus-udp");
// Create the animation object and configure for specified output
	// AnimationInterface anim (animFile);
	// anim.SetXMLOutput ();
//anim.StartAnimation ();
// to get current time
	gettimeofday(&detail_time2, NULL);
	double t2 = detail_time2.tv_sec + (detail_time2.tv_usec/1000000.0);
	NS_LOG_INFO ("Run Simulation.");
	Simulator::Run ();
	Simulator::Destroy ();
	NS_LOG_INFO ("Done.");
// print total time elapsed in microsecond
	gettimeofday(&detail_time3, NULL);
	double t3 = detail_time3.tv_sec + (detail_time3.tv_usec/1000000.0);
	NS_LOG_INFO ("Total (ms): " << (t3 - t1)*1000);
	NS_LOG_INFO ("Modbus Simulation time (ms): " << (t3 - t2)*1000);
	NS_LOG_INFO ("Node and Link creation (ms): " << (t2 - t1)*1000);
	return 0;
}