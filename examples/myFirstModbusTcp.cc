/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/netanim-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/modbus-tcp-helper.h"
#include "ns3/modbus-mgr.h"
// Network Topology
//
// n0 n1
//  | |
// ======
// LAN 10.1.1.0
//
// How to Run
//
// Example 1: Run MODBUS TCP with verbose logging:
// ./waf --run "scratch/myfirstModbusTcp --verbose=1"
//
// MODBUS requests
uint8_t ENCAP_INTERFACE_TRANS_CANOPEN_GENERAL_REF_REQ_RSP_PDU[] = {0x2B, 0x0D};
uint8_t ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__BASIC_DEVICE_ID_OBJ_ID_00[] = {0x2B, 0x0E, 0x01, 0x00};
uint8_t ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__REGULAR_DEVICE_ID_OBJ_ID_00[] = {0x2B, 0x0E, 0x02, 0x00};
uint8_t ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__EXTENDED_DEVICE_ID_OBJ_ID_00[] = {0x2B, 0x0E, 0x03, 0x00};
uint8_t ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__SPECIFIC_DEVICE_ID_OBJ_ID_00[] = {0x2B, 0x0E, 0x04, 0x00};

using namespace ns3;
NS_LOG_COMPONENT_DEFINE ("FirstModbusTcpScriptExample");
int
main (int argc, char *argv[])
{
	bool verbose = false;
	// std::string animFile = "first-modbus-tcp-animfile.xml";
// under ns-3-dev type ./waf --run 'scratch/myfirst --PrintHelp'
// to see a list of parameters can be passed to the parser
	CommandLine cmd;
	cmd.AddValue ("verbose", "Tell MODBUS applications to log if true", verbose);
	// cmd.AddValue ("animFile", "File Name for Animation Output", animFile);
	cmd.Parse (argc, argv);
	if (verbose)
	{
		LogComponentEnable ("ModbusTcpServer", LOG_LEVEL_INFO);
		LogComponentEnable ("ModbusTcpClient", LOG_LEVEL_INFO);
	}
	NodeContainer nodes;
	nodes.Create (2);
	PointToPointHelper pointToPoint;
	pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
	pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));
	NetDeviceContainer devices;
	devices = pointToPoint.Install (nodes);
	InternetStackHelper stack;
	stack.Install (nodes);
	Ipv4AddressHelper address;
	address.SetBase ("10.1.1.0", "255.255.255.0");
	Ipv4InterfaceContainer interfaces = address.Assign (devices);
	ModbusTcpServerHelper cModbusTcpServerHelper;
	ApplicationContainer serverApps = cModbusTcpServerHelper.Install (nodes.Get (1));
	serverApps.Start (Seconds (1.0));
	serverApps.Stop (Seconds (10.0));
	ModbusTcpClientHelper cModbusTcpClientHelper (interfaces.GetAddress (1));
	cModbusTcpClientHelper.SetAttribute ("MaxPackets", UintegerValue (1));
	cModbusTcpClientHelper.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
	ApplicationContainer clientApps = cModbusTcpClientHelper.Install (nodes.Get (0));
// set MODBUS requests
	cModbusTcpClientHelper.SetRequest (clientApps.Get(0), Seconds (0.1),
	                                   ENCAP_INTERFACE_TRANS_CANOPEN_GENERAL_REF_REQ_RSP_PDU);
	cModbusTcpClientHelper.SetRequest (clientApps.Get(0), Seconds (0.2),
	                                   ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__BASIC_DEVICE_ID_OBJ_ID_00);
	cModbusTcpClientHelper.SetRequest (clientApps.Get(0), Seconds (0.1),
	                                   ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__SPECIFIC_DEVICE_ID_OBJ_ID_00);
	cModbusTcpClientHelper.SetCount (clientApps.Get(0), 100);
	clientApps.Start (Seconds (2.0));
	clientApps.Stop (Seconds (10.0));
//configure tracing
	AsciiTraceHelper ascii;
	pointToPoint.EnableAsciiAll (ascii.CreateFileStream ("first-modbus-tcp.tr"));
	pointToPoint.EnablePcapAll ("first-modbus-tcp");
// Create the animation object and configure for specified output
	// AnimationInterface anim (animFile);
	// anim.SetXMLOutput ();
//anim.StartAnimation ();
	Simulator::Run ();
	Simulator::Destroy ();
	return 0;
}