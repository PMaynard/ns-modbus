/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/netanim-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/modbus-tcp-helper.h"
#include "ns3/modbus-mgr.h"
#include "ns3/point-to-point-layout-module.h"
// Network topology (default)
//
// How to Run
//
// set NS_LOG if required
// export NS_LOG=StarModbusTcp=level_all
//
// Example 1: Run star topology MODBUS TCP with verbose logging:
// ./waf --run "scratch/starModbusTcp --verbose=1"
//
// Example 2: Run star topology MODBUS TCP with 10 MODBUS clients:
// ./waf --run "scratch/starModbusTcp --verbose=0 --nSpokes=10"
//
// MODBUS requests
uint8_t ENCAP_INTERFACE_TRANS_CANOPEN_GENERAL_REF_REQ_RSP_PDU[] = {0x2B,0x0D};
uint8_t ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__BASIC_DEVICE_ID_OBJ_ID_00[] = {0x2B, 0x0E, 0x01, 0x00};
uint8_t ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__REGULAR_DEVICE_ID_OBJ_ID_00[] = {0x2B, 0x0E, 0x02, 0x00};
uint8_t ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__EXTENDED_DEVICE_ID_OBJ_ID_00[] = {0x2B, 0x0E, 0x03, 0x00};
uint8_t ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__SPECIFIC_DEVICE_ID_OBJ_ID_00[] = {0x2B, 0x0E, 0x04, 0x00};

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("StarModbusTcp");
int
main (int argc, char *argv[])
{
	struct timeval detail_time1, detail_time2, detail_time3;
// to get current time
	gettimeofday(&detail_time1, NULL);
	double t1 = detail_time1.tv_sec + (detail_time1.tv_usec/1000000.0);
	bool verbose = false;
	// std::string animFile = "star-modbus-tcp-animfile.xml";
//
// Default number of nodes in the star. Overridable by command line argument.
//
	uint32_t nSpokes = 8;
// under ns-3-dev type ./waf --run 'scratch/myfirst --PrintHelp'
// to see a list of parameters can be passed to the parser
	CommandLine cmd;
	cmd.AddValue ("verbose", "Tell MODBUS applications to log if true", verbose);
	cmd.AddValue ("nSpokes", "Number of MODBUS star clients", nSpokes);
	// cmd.AddValue ("animFile", "File Name for Animation Output", animFile);
	cmd.Parse (argc, argv);
	if (verbose)
	{
		LogComponentEnable ("ModbusTcpServer", LOG_LEVEL_INFO);
		LogComponentEnable ("ModbusTcpClient", LOG_LEVEL_INFO);
	}
	NS_LOG_INFO ("Build star topology.");
	PointToPointHelper pointToPoint;
	pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
	pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));
	PointToPointStarHelper star (nSpokes, pointToPoint);
	NS_LOG_INFO ("Install internet stack on all nodes.");
	InternetStackHelper internet;
	star.InstallStack (internet);
	NS_LOG_INFO ("Assign IP Addresses.");
	star.AssignIpv4Addresses (Ipv4AddressHelper ("10.1.1.0", "255.255.255.0"));
//Ipv4InterfaceContainer interfaces = address.Assign (devices);
	NS_LOG_INFO ("Create applications.");
//
// Create a MODBUS TCP server on the star "hub"
//
	ModbusTcpServerHelper cModbusTcpServerHelper;
	ApplicationContainer serverApps = cModbusTcpServerHelper.Install (star.GetHub ());
	serverApps.Start (Seconds (1.0));
	serverApps.Stop (Seconds (15.0));
//
// Create MODBUS TCP client applications send TCP to the hub,
// one on each spoke node
//
	ApplicationContainer spokeApps;
	for (uint32_t i = 0; i < star.SpokeCount (); ++i)
	{
		ModbusTcpClientHelper cModbusTcpClientHelper (star.GetHubIpv4Address (i));
		cModbusTcpClientHelper.SetAttribute ("MaxPackets", UintegerValue (1));
		cModbusTcpClientHelper.SetAttribute ("Interval", TimeValue (Seconds (1.0)));
		ApplicationContainer clientApps = cModbusTcpClientHelper.Install (star.GetSpokeNode (i));
		spokeApps.Add (clientApps);
// set MODBUS requests
		cModbusTcpClientHelper.SetRequest (clientApps.Get(0), Seconds (0.1),
		                                   ENCAP_INTERFACE_TRANS_CANOPEN_GENERAL_REF_REQ_RSP_PDU);
		cModbusTcpClientHelper.SetRequest (clientApps.Get(0), Seconds (0.2),
		                                   ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__BASIC_DEVICE_ID_OBJ_ID_00);
		cModbusTcpClientHelper.SetRequest (clientApps.Get(0), Seconds (0.1),
		                                   ENCAP_INTERFACE_TRANS_READ_DEVICE_ID__SPECIFIC_DEVICE_ID_OBJ_ID_00);
		cModbusTcpClientHelper.SetCount (clientApps.Get(0), 2);
	}
	spokeApps.Start (Seconds (2.0));
	spokeApps.Stop (Seconds (10.0));
	NS_LOG_INFO ("Enable static global routing.");
//
// Turn on global static routing so we can actually be routed across the star.
//
	Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
	NS_LOG_INFO ("Enable pcap tracing.");
//configure tracing
	AsciiTraceHelper ascii;
	pointToPoint.EnableAsciiAll (ascii.CreateFileStream ("star-modbus-tcp.tr"));
	pointToPoint.EnablePcapAll ("star-modbus-tcp");
// Create the animation object and configure for specified output
	// AnimationInterface anim (animFile);
	// anim.SetXMLOutput ();
//anim.StartAnimation ();
// to get current time
	gettimeofday(&detail_time2, NULL);
	double t2 = detail_time2.tv_sec + (detail_time2.tv_usec/1000000.0);
	NS_LOG_INFO ("Run Simulation.");
	Simulator::Run ();
	Simulator::Destroy ();
	NS_LOG_INFO ("Done.");
// print total time elapsed in microsecond
	gettimeofday(&detail_time3, NULL);
	double t3 = detail_time3.tv_sec + (detail_time3.tv_usec/1000000.0);
	NS_LOG_INFO ("Total (ms): " << (t3 - t1)*1000);
	NS_LOG_INFO ("Modbus Simulation time (ms): " << (t3 - t2)*1000);
	NS_LOG_INFO ("Node and Link creation (ms): " << (t2 - t1)*1000);
	return 0;
}