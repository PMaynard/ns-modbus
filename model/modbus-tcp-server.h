/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#ifndef MODBUS_TCP_SERVER_H
#define MODBUS_TCP_SERVER_H
#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/address.h"
#include "ns3/packet-loss-counter.h"
#include "ns3/modbus-mgr.h"
namespace ns3 {
/**
 * \ingroup applications
 * \defgroup modbustcpclientserver modbustcpclientserver
 */
/**
 * \ingroup tcpclientserver
 * \class ModbusTcpServer
 * \brief Create a server application which waits for input MODBUS TCP request
 *
   and generates appropriate response according to the function code.
 *
   It also uses the information carried into their payload of the packet
 *
   to compute delay and to determine if some packets are lost.
 * \brief A MODBUS TCP server. Receives MODBUS TCP request from a remote host
 *
   and generates appropriate response according to the function code.
 *
   It also uses the information carried into their payload of the packet
 *
   to compute delay and to determine if some packets are lost.
 */
class ModbusTcpServer : public Application
{
public:
static TypeId GetTypeId (void);
ModbusTcpServer ();
virtual ~ModbusTcpServer ();
/**
 * returns the number of lost packets
 * \return the number of lost packets
 */
uint32_t GetLost (void) const;
/**
 * \brief returns the number of received packets
 * \return the number of received packets
 */
uint32_t GetReceived (void) const;
/**
 * \return the size of the window used for checking loss.
 */
uint16_t GetPacketWindowSize () const;
/**
 * \brief Set the size of the window used for checking loss. This value should
 * be a multiple of 8
 * \param size the size of the window used for checking loss. This value should
 * be a multiple of 8
 */
void SetPacketWindowSize (uint16_t size);
/**
 * \return pointer to listening socket
 */
Ptr<Socket> GetListeningSocket (void) const;
/**
 * \return list of pointers to accepted sockets
 */
std::list<Ptr<Socket> > GetAcceptedSockets (void) const;
protected:
virtual void DoDispose (void);
private:
virtual void StartApplication (void);
virtual void StopApplication (void);
void HandleRead (Ptr<Socket> socket);
void HandleAccept (Ptr<Socket>, const Address& from);
void HandlePeerClose (Ptr<Socket>);
void HandlePeerError (Ptr<Socket>);
uint16_t m_port;
Ptr<Socket> m_socket;
ModbusMgr m_cModbusMgr;
std::list<Ptr<Socket> > m_socketList; // the accepted sockets
Address m_local;
uint32_t m_received;
PacketLossCounter m_lossCounter;
};
} // namespace ns3
#endif /* MODBUS_TCP_SERVER_H */
