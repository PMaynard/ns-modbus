/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#include "modbus-mgr.h"
namespace ns3 {
ModbusMgr::ModbusMgr ()
{
	m_rspPdu = new uint8_t [PDU_SIZE];
	m_coilsStatus = new uint8_t [MAX_COILS_NUM];
	m_inputsStatus = new uint8_t [MAX_INPUTS_NUM];
	m_holdingRegisters = new uint16_t [MAX_HOLDING_REGISTERS];
	m_inputRegisters = new uint16_t [MAX_INPUT_REGISTERS];
	m_exceptionStatusRegister = 0;
	m_listenOnlyMode = false;
	m_communicationEventLog = std::string ();
	m_diagnosticRegister = 0;
	m_endOfMessageDelimiter = std::string (1, 0x0A);
	m_busMessageCounter = 0;
	m_busCommunicationErrorCounter = 0;
	m_busExceptionErrorCounter = 0;
	m_slaveMessageCounter = 0;
	m_slaveNoResponseCounter = 0;
	m_slaveNAKCounter = 0;
	m_slaveBusyCounter = 0;
	m_busCharacterOverrunCounter = 0;
	m_commEventCounter = 0;
	m_EventCounter = 0;
	m_events = new uint8_t [MAX_EVENTS];
	m_fileRecord = new uint16_t *[MAX_FILE_NUMBER]; // rows
	for (uint32_t i = 0; i < MAX_FILE_NUMBER; ++i)
	{
		m_fileRecord[i] = new uint16_t [1000]; // cols
	}
	m_fifo = new uint16_t [MAX_FIFO_QUEUE_SIZE];
	m_fifoCount = 0;
}
ModbusMgr::~ModbusMgr ()
{
	delete [] m_rspPdu; // release response PDU
	m_rspPdu = 0;
	delete [] m_coilsStatus; // release coils status
	m_coilsStatus = 0;
	delete [] m_inputsStatus; // release inputs status
	m_inputsStatus = 0;
	delete [] m_holdingRegisters; // release holding registers
	m_holdingRegisters = 0;
	delete [] m_inputRegisters; // release holding registers
	m_inputRegisters = 0;
	delete [] m_events; // release event logs
	m_events = 0;
	for (uint32_t i = 0; i < MAX_FILE_NUMBER; ++i)
	{
		delete [] m_fileRecord[i];
	}
	delete [] m_fileRecord;
	m_fileRecord = 0;
	delete [] m_fifo;
	m_fifo = 0;
//delete [] READ_DEVICE_ID__BASIC_DEVICE_ID_OBJ_ID_00;
}
/**
 * \ingroup modbus
 * \class ModbusMgr
 * \brief Generates MODBUS response. Returns NULL if no response generated
 */
uint8_t*
ModbusMgr::DoMain (uint8_t * pdu)
{
// increment bus message count
	++m_busMessageCounter;
// increment slave message count
	++m_slaveMessageCounter;
// increment slave no response count
	if (m_listenOnlyMode)
	{
		++m_slaveNoResponseCounter;
	}
// store remote device MODBUS receive event
	(m_listenOnlyMode) ?
	PushEventLog
	        (REMOTE_DEVICE_MODBUS_RECEIVE_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_RECEIVE_EVENT);
	uint8_t functionCode = pdu[0];
	switch (functionCode)
	{
	case READ_COILS:
	{
		std::cout << "Read Coils " << std::endl;
		uint16_t startAdr;
		uint16_t quantityOfCoils;
		startAdr = (pdu[1] << 8) + pdu[2];
		quantityOfCoils = (pdu[3] << 8) + pdu[4];
		return (m_listenOnlyMode) ? NULL : ReadCoils (startAdr, quantityOfCoils);
	}
	case READ_DISCRETE_INPUTS:
	{
		std::cout << "Read Discrete Inputs " << std::endl;
		uint16_t startAdr;
		uint16_t quantityOfInputs;
		startAdr = (pdu[1] << 8) + pdu[2];
		quantityOfInputs = (pdu[3] << 8) + pdu[4];
		return (m_listenOnlyMode) ? NULL : ReadDiscreteInputs (startAdr, quantityOfInputs);
	}
	case READ_HOLDING_REGISTERS:
	{
		std::cout << "Read Holding Registers " << std::endl;
		uint16_t startAdr;
		uint16_t quantityOfRegisters;
		startAdr = (pdu[1] << 8) + pdu[2];
		quantityOfRegisters = (pdu[3] << 8) + pdu[4];
		return (m_listenOnlyMode) ? NULL : ReadHoldingRegisters (startAdr, quantityOfRegisters);
	}
	case READ_INPUT_REGISTERS:
	{
		std::cout << "Read Input Registers " << std::endl;
		uint16_t startAdr;
		uint16_t quantityOfRegisters;
		startAdr = (pdu[1] << 8) + pdu[2];
		quantityOfRegisters = (pdu[3] << 8) + pdu[4];
		return (m_listenOnlyMode) ? NULL : ReadInputRegisters (startAdr, quantityOfRegisters);
	}
	case WRITE_SINGLE_COIL:
	{
		std::cout << "Write Single Coil " << std::endl;
		uint16_t outputAdr;
		uint16_t outputValue;
		outputAdr = (pdu[1] << 8) + pdu[2];
		outputValue = (pdu[3] << 8) + pdu[4];
		return (m_listenOnlyMode) ? NULL : WriteSingleCoil (outputAdr, outputValue);
	}
	case WRITE_SINGLE_REGISTER:
	{
		std::cout << "Write Single Register " << std::endl;
		uint16_t registerAdr;
		uint16_t registerValue;
		registerAdr = (pdu[1] << 8) + pdu[2];
		registerValue = (pdu[3] << 8) + pdu[4];
		return (m_listenOnlyMode) ? NULL : WriteSingleRegister (registerAdr, registerValue);
	}
	case READ_EXCEPTION_STATUS:
	{
		std::cout << "Read Exception Status " << std::endl;
		return (m_listenOnlyMode) ? NULL : ReadExceptionStatus ();
	}
	case DIAGNOSTICS:
	{
		std::cout << "Diagnostics, ";
		uint16_t subfunctionCode;
		subfunctionCode = (pdu[1] << 8) + pdu[2];
		bool listenOnlyMode;
		listenOnlyMode = m_listenOnlyMode;
		return (listenOnlyMode) ? NULL : Diagnostics (subfunctionCode, pdu);
	}
	case GET_COMM_EVENT_COUNTER:
	{
		std::cout << "Get Comm Event Counter " << std::endl;
		return (m_listenOnlyMode) ? NULL : GetCommEventCounter ();
	}
	case GET_COMM_EVENT_LOG:
	{
		std::cout << "Get Comm Event Log " << std::endl;
		return (m_listenOnlyMode) ? NULL : GetCommEventLog ();
	}
	case WRITE_MULTIPLE_COILS:
	{
		std::cout << "Write Multiple Coils " << std::endl;
		uint16_t startAdr;
		uint16_t quantityOfOutputs;
		uint8_t byteCount;
		startAdr = (pdu[1] << 8) + pdu[2];
		quantityOfOutputs = (pdu[3] << 8) + pdu[4];
		byteCount = pdu[5];
		return (m_listenOnlyMode) ? NULL : WriteMultipleCoils (startAdr, quantityOfOutputs, byteCount, pdu);
	}
	case WRITE_MULTIPLE_REGISTERS:
	{
		std::cout << "Write Multiple Registers " << std::endl;
		uint16_t startAdr;
		uint16_t quantityOfRegisters;
		uint8_t byteCount;
		startAdr = (pdu[1] << 8) + pdu[2];
		quantityOfRegisters = (pdu[3] << 8) + pdu[4];
		byteCount = pdu[5];
		return (m_listenOnlyMode) ? NULL : WriteMultipleRegisters (startAdr, quantityOfRegisters,
		                                                           byteCount, pdu);
	}
	case REPORT_SLAVE_ID:
	{
		std::cout << "Report Slave ID " << std::endl;
		return (m_listenOnlyMode) ? NULL : ReportSlaveID ();
	}
	case READ_FILE_RECORD:
	{
		std::cout << "Read File Record " << std::endl;
		uint8_t byteCount;
		byteCount = pdu[1];
		return (m_listenOnlyMode) ? NULL : ReadFileRecord (byteCount, pdu);
	}
	case WRITE_FILE_RECORD:
	{
		std::cout << "Write File Record " << std::endl;
		uint8_t requestDataLength;
		requestDataLength = pdu[1];
		return (m_listenOnlyMode) ? NULL : WriteFileRecord (requestDataLength, pdu);
	}
	case MASK_WRITE_REGISTER:
	{
		std::cout << "Mask Write Register " << std::endl;
		uint16_t referenceAdr;
		uint16_t and_mask;
		uint16_t or_mask;
		referenceAdr = (pdu[1] << 8) + pdu[2];
		and_mask = (pdu[3] << 8) + pdu[4];
		or_mask = (pdu[5] << 8) + pdu[6];
		return (m_listenOnlyMode) ? NULL : MaskWriteRegister (referenceAdr, and_mask, or_mask);
	}
	case READ_WRITE_MULTIPLE_REGISTERS:
	{
		std::cout << "Read/Write Multiple Registers " << std::endl;
		uint16_t readStartAdr;
		uint16_t quantityToRead;
		uint16_t writeStartAdr;
		uint16_t quantityToWrite;
		uint8_t writeByteCount;
		readStartAdr = (pdu[1] << 8) + pdu[2];
		quantityToRead = (pdu[3] << 8) + pdu[4];
		writeStartAdr = (pdu[5] << 8) + pdu[6];
		quantityToWrite = (pdu[7] << 8) + pdu[8];
		writeByteCount = pdu[9];
		return
		        (m_listenOnlyMode) ?
		        NULL :
		        ReadWriteMultipleRegisters (readStartAdr,
		                                    quantityToRead,
		                                    writeStartAdr, quantityToWrite, writeByteCount, pdu);
	}
	case READ_FIFO_QUEUE:
	{
		std::cout << "Read FIFO Queue " << std::endl;
		uint16_t fifoPointerAdr;
		fifoPointerAdr = (pdu[1] << 8) + pdu[2];
		return (m_listenOnlyMode) ? NULL : ReadFifoQueue (fifoPointerAdr);
	}
	case ENCAPSULATED_INTERFACE_TRANSPORT:
	{
		std::cout << "Encapsulated Interface Transport " << std::endl;
		uint8_t meiType;
		meiType = pdu[1];
		return (m_listenOnlyMode) ? NULL : encapsulatedInterfaceTransport (meiType, pdu);
	}
	default:
	{
		std::cout << "error: undefined function code " << std::endl;
		m_rspPdu[0] = pdu[0] + 0x80;
		m_rspPdu[1] = 0x01; // exception code 0x01, function code not supported
		return (m_listenOnlyMode) ? NULL : m_rspPdu;
	}
	}
	return m_rspPdu;
}
void
ModbusMgr::PushEventLog (uint8_t event)
{
	if (m_EventCounter < MAX_EVENTS)
	{
		++m_EventCounter;
	}
// push down the old events
	for (uint16_t i = m_EventCounter-1; i >= 1; --i)
	{
		m_events[i] = m_events[i-1];
	}
	m_events[0] = event;
}
uint8_t*
ModbusMgr::ReadCoils (uint16_t startAdr, uint16_t quantityOfCoils)
{
	std::cout << "startAdr: " << startAdr << std::endl;
	std::cout << "quantityOfCoils: " << quantityOfCoils << std::endl;
	if ((0x0001 > quantityOfCoils) || (0x07D0 < quantityOfCoils))
	{
		m_rspPdu[0] = READ_COILS + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if (0xFFFF < startAdr + quantityOfCoils)
	{
		m_rspPdu[0] = READ_COILS + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	m_rspPdu[0] = READ_COILS;
	uint8_t byteCount = quantityOfCoils / 8;
	if (0 != (quantityOfCoils % 8))
	{
		++byteCount;
	}
	m_rspPdu[1] = byteCount;
// read coils
	try
	{
		for (uint16_t i = startAdr; i < (quantityOfCoils + startAdr); ++i)
		{
			uint8_t coilsStatusRspByteIndex = i / 8;
			uint8_t coilsStatusRspBitIndex = i % 8;
			if (0 == (0x01 & m_coilsStatus[i])) // LSb: 0 = OFF and 1 = ON
			{
				switch (coilsStatusRspBitIndex)
				{
				case 0:
					m_rspPdu[2 + coilsStatusRspByteIndex] &= 0xFE;
					break;
				case 1:
					m_rspPdu[2 + coilsStatusRspByteIndex] &= 0xFD;
					break;
				case 2:
					m_rspPdu[2 + coilsStatusRspByteIndex] &= 0xFB;
					break;
				case 3:
					m_rspPdu[2 + coilsStatusRspByteIndex] &= 0xF7;
					break;
				case 4:
					m_rspPdu[2 + coilsStatusRspByteIndex] &= 0xEF;
					break;
				case 5:
					m_rspPdu[2 + coilsStatusRspByteIndex] &= 0xDF;
					break;
				case 6:
					m_rspPdu[2 + coilsStatusRspByteIndex] &= 0xBF;
					break;
				case 7:
					m_rspPdu[2 + coilsStatusRspByteIndex] &= 0x7F;
					break;
				}
			}
			else
			{
				switch (coilsStatusRspBitIndex)
				{
				case 0:
					m_rspPdu[2 + coilsStatusRspByteIndex] |= 0x01;
					break;
				case 1:
					m_rspPdu[2 + coilsStatusRspByteIndex] |= 0x02;
					break;
				case 2:
					m_rspPdu[2 + coilsStatusRspByteIndex] |= 0x04;
					break;
				case 3:
					m_rspPdu[2 + coilsStatusRspByteIndex] |= 0x08;
					break;
				case 4:
					m_rspPdu[2 + coilsStatusRspByteIndex] |= 0x10;
					break;
				case 5:
					m_rspPdu[2 + coilsStatusRspByteIndex] |= 0x20;
					break;
				case 6:
					m_rspPdu[2 + coilsStatusRspByteIndex] |= 0x40;
					break;
				case 7:
					m_rspPdu[2 + coilsStatusRspByteIndex] |= 0x80;
					break;
				}
			}
		}
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = READ_COILS + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}

uint8_t*
ModbusMgr::ReadDiscreteInputs (uint16_t startAdr, uint16_t quantityOfInputs)
{
	std::cout << "startAdr: " << startAdr << std::endl;
	std::cout << "quantityOfInputs: " << quantityOfInputs << std::endl;
	if ((0x0001 > quantityOfInputs) || (0x07D0 < quantityOfInputs))
	{
		m_rspPdu[0] = READ_DISCRETE_INPUTS + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if (0xFFFF < startAdr + quantityOfInputs)
	{
		m_rspPdu[0] = READ_DISCRETE_INPUTS + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	m_rspPdu[0] = READ_DISCRETE_INPUTS;
	uint8_t byteCount = quantityOfInputs / 8;
	if (0 != (quantityOfInputs % 8))
	{
		++byteCount;
	}
	m_rspPdu[1] = byteCount;
// read discrete inputs
	try
	{
		for (uint16_t i = startAdr; i < (quantityOfInputs + startAdr); ++i)
		{
			uint8_t inputStatusRspByteIndex = i / 8;
			uint8_t inputStatusRspBitIndex = i % 8;
			if (0 == (0x01 & m_inputsStatus[i])) // LSb: 0 = OFF and 1 = ON
			{
				switch (inputStatusRspBitIndex)
				{
				case 0:
					m_rspPdu[2 + inputStatusRspByteIndex] &= 0x7F;
					break;
				case 1:
					m_rspPdu[2 + inputStatusRspByteIndex] &= 0xBF;
					break;
				case 2:
					m_rspPdu[2 + inputStatusRspByteIndex] &= 0xDF;
					break;
				case 3:
					m_rspPdu[2 + inputStatusRspByteIndex] &= 0xEF;
					break;
				case 4:
					m_rspPdu[2 + inputStatusRspByteIndex] &= 0xF7;
					break;
				case 5:
					m_rspPdu[2 + inputStatusRspByteIndex] &= 0xFB;
					break;
				case 6:
					m_rspPdu[2 + inputStatusRspByteIndex] &= 0xFD;
					break;
				case 7:
					m_rspPdu[2 + inputStatusRspByteIndex] &= 0xFE;
					break;
				}
			}
			else
			{
				switch (inputStatusRspBitIndex)
				{
				case 0:
					m_rspPdu[2 + inputStatusRspByteIndex] |= 0x80;
					break;
				case 1:
					m_rspPdu[2 + inputStatusRspByteIndex] |= 0x40;
					break;
				case 2:
					m_rspPdu[2 + inputStatusRspByteIndex] |= 0x20;
					break;
				case 3:
					m_rspPdu[2 + inputStatusRspByteIndex] |= 0x10;
					break;
				case 4:
					m_rspPdu[2 + inputStatusRspByteIndex] |= 0x08;
					break;
				case 5:
					m_rspPdu[2 + inputStatusRspByteIndex] |= 0x04;
					break;
				case 6:
					m_rspPdu[2 + inputStatusRspByteIndex] |= 0x02;
					break;
				case 7:
					m_rspPdu[2 + inputStatusRspByteIndex] |= 0x01;
					break;
				}
			}
		}
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = READ_DISCRETE_INPUTS + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}
uint8_t*
ModbusMgr::ReadHoldingRegisters (uint16_t startAdr, uint16_t quantityOfRegisters)
{
	std::cout << "startAdr: " << startAdr << std::endl;
	std::cout << "quantityOfRegisters: " << quantityOfRegisters << std::endl;
	if ((0x0001 > quantityOfRegisters) || (0x007D < quantityOfRegisters))
	{
		m_rspPdu[0] = READ_HOLDING_REGISTERS + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if (0xFFFF < startAdr + quantityOfRegisters)
	{
		m_rspPdu[0] = READ_HOLDING_REGISTERS + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
// read holding registers
	try
	{
		for (uint16_t i = startAdr, rspIndex = 2; i < (quantityOfRegisters + startAdr); ++i, rspIndex+=2)
		{
			m_rspPdu[rspIndex] = (m_holdingRegisters[i] & 0xff00) >> 8;
			m_rspPdu[rspIndex+1] = (m_holdingRegisters[i] & 0xff);
		}
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = READ_HOLDING_REGISTERS + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
	m_rspPdu[0] = READ_HOLDING_REGISTERS;
	m_rspPdu[1] = quantityOfRegisters * 2;
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}
uint8_t*
ModbusMgr::ReadInputRegisters (uint16_t startAdr, uint16_t quantityOfRegisters)
{
	std::cout << "startAdr: " << startAdr << std::endl;
	std::cout << "quantityOfRegisters: " << quantityOfRegisters << std::endl;
	if ((0x0001 > quantityOfRegisters) || (0x007D < quantityOfRegisters))
	{
		m_rspPdu[0] = READ_INPUT_REGISTERS + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if (0xFFFF < startAdr + quantityOfRegisters)
	{
		m_rspPdu[0] = READ_INPUT_REGISTERS + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
// read input registers
	try
	{
		for (uint16_t i = startAdr, rspIndex = 2; i < (quantityOfRegisters + startAdr); ++i, rspIndex+=2)
		{
			m_rspPdu[rspIndex] = (m_inputRegisters[i] & 0xff00) >> 8;
			m_rspPdu[rspIndex+1] = (m_inputRegisters[i] & 0xff);
		}
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = READ_INPUT_REGISTERS + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
	m_rspPdu[0] = READ_INPUT_REGISTERS;
	m_rspPdu[1] = quantityOfRegisters * 2;
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}
uint8_t*
ModbusMgr::WriteSingleCoil (uint16_t outputAdr, uint16_t outputValue)
{
	std::cout << "outputAdr: " << outputAdr << std::endl;
	std::cout << "outputValue: " << outputValue << std::endl;
	if ((0x0000 != outputValue) && (0xFF00 != outputValue))
	{
		m_rspPdu[0] = WRITE_SINGLE_COIL + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if (0xFFFF < outputAdr) // added for completeness (uint16_t)
	{
		m_rspPdu[0] = WRITE_SINGLE_COIL + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
// set the value. LSb: 0 = OFF and 1 = ON
	try
	{
		if (0x0000 == outputValue)
		{
// set OFF
			m_coilsStatus[outputAdr] = 0;
		}
		else
		{
// set ON
			m_coilsStatus[outputAdr] = 1;
		}
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = WRITE_SINGLE_COIL + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
// the response is an echo of the request
	m_rspPdu[0] = WRITE_SINGLE_COIL;
	m_rspPdu[1] = (outputAdr & 0xff00) >> 8;
	m_rspPdu[2] = (outputAdr & 0xff);
	m_rspPdu[3] = (outputValue & 0xff00) >> 8;
	m_rspPdu[4] = (outputValue & 0xff);
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}
uint8_t*
ModbusMgr::WriteSingleRegister (uint16_t registerAdr, uint16_t registerValue)
{
	std::cout << "registerAdr: " << registerAdr << std::endl;
	std::cout << "registerValue: " << registerValue << std::endl;
	if ((0x0000 > registerValue) || (0xFFFF < registerValue)) // added for completeness (uint16_t)
	{
		m_rspPdu[0] = WRITE_SINGLE_REGISTER + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if (0xFFFF < registerAdr) // added for completeness (uint16_t)
	{
		m_rspPdu[0] = WRITE_SINGLE_REGISTER + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
// set the register value
	try
	{
		m_holdingRegisters[registerAdr] = registerValue;
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = WRITE_SINGLE_REGISTER + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
// the response is an echo of the request
	m_rspPdu[0] = WRITE_SINGLE_REGISTER;
	m_rspPdu[1] = (registerAdr & 0xff00) >> 8;
	m_rspPdu[2] = (registerAdr & 0xff);
	m_rspPdu[3] = (registerValue & 0xff00) >> 8;
	m_rspPdu[4] = (registerValue & 0xff);
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}
uint8_t*
ModbusMgr::ReadExceptionStatus ()
{
// read exception status
	try
	{
		m_rspPdu[0] = READ_EXCEPTION_STATUS;
		m_rspPdu[1] = m_exceptionStatusRegister;
		++m_commEventCounter; // increment for each successful message completion
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
		return m_rspPdu;
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = READ_EXCEPTION_STATUS + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
}
uint8_t*
ModbusMgr::Diagnostics (uint16_t subfunctionCode, uint8_t * pdu)
{
	std::cout << "subfunctionCode: 0x" << std::hex << subfunctionCode << " " << std::dec;
	try
	{
		switch (subfunctionCode)
		{
		case RETURN_QUERY_DATA:
		{
			std::cout << "Return query data " << std::endl;
			for (uint16_t i = 0; i < MAX_MODBUS_PDU; ++i)
			{
				m_rspPdu[i] = pdu[i];
			}
			++m_commEventCounter; // increment for each successful message completion
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		case RESTART_COMMUNICATION_OPTION:
		{
			std::cout << "Restart communication option " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			switch (requestDataField)
			{
			case 0xFF00:
			{
				m_communicationEventLog.clear ();
				m_commEventCounter = 0;
// create an echo response
				for (uint16_t i = 0; i < 5; ++i)
				{
					m_rspPdu[i] = pdu[i];
				}
				break;
			}
			case 0x0000:
			{
// create an echo response
				for (uint16_t i = 0; i < 5; ++i)
				{
					m_rspPdu[i] = pdu[i];
				}
				++m_commEventCounter; // increment for each successful message completion
				break;
			}
			default:
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
			}
			m_listenOnlyMode = false; // the only function that brings the port out of Listen Only Mode
			PushEventLog (REMOTE_DEVICE_INITIATED_COMMUNICATION_RESTART);
			return m_rspPdu;
		}
		case RETURN_DIAGNOSTICS_REGISTER:
		{
			std::cout << "Return diagnostics registers " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			switch (requestDataField)
			{
			case 0x0000:
			{
// get diagnostic register contents
				m_rspPdu[0] = DIAGNOSTICS;
				m_rspPdu[1] = (RETURN_DIAGNOSTICS_REGISTER & 0xff00) >> 8;
				m_rspPdu[2] = (RETURN_DIAGNOSTICS_REGISTER & 0xff);
				m_rspPdu[3] = (m_diagnosticRegister & 0xff00) >> 8;
				m_rspPdu[4] = (m_diagnosticRegister & 0xff);
				++m_commEventCounter; // increment for each successful message completion
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
				break;
			}
			default:
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				break;
			}
			}
			return m_rspPdu;
		}
		case CHANGE_ASCII_INPUT_DELIMITER:
		{
			std::cout << "Change ASCII input delimiter " << std::endl;
			if (0x00 != pdu[4])
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03

				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
// change end of message delimiter
			m_endOfMessageDelimiter = std::string (1, pdu[3]);
// echo request data
			for (uint16_t i = 0; i < 5; ++i)
			{
				m_rspPdu[i] = pdu[i];
			}
			++m_commEventCounter; // increment for each successful message completion
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		case FORCE_LISTEN_ONLY:
		{
			std::cout << "Force listen only " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			if (0x0000 != requestDataField)
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
			m_listenOnlyMode = true;
			++m_commEventCounter; // increment for each successful message completion
			PushEventLog (REMOTE_DEVICE_ENTERED_LISTEM_ONLY_MODE);
			return NULL;
		}
		case CLEAR_COUNTERS_AND_DIAGNOSTICS_REGISTER:
		{
			std::cout << "Clear counters and diagnostics register " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			if (0x0000 != requestDataField)
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
			m_diagnosticRegister = 0;
			m_busMessageCounter = 0;
			m_busCommunicationErrorCounter = 0;
			m_busExceptionErrorCounter = 0;
			m_slaveMessageCounter = 0;
			m_slaveNoResponseCounter = 0;
			m_slaveNAKCounter = 0;
			m_slaveBusyCounter = 0;
			m_busCharacterOverrunCounter = 0;
			m_commEventCounter = 0;

// echo request data
			for (uint16_t i = 0; i < 5; ++i)
			{
				m_rspPdu[i] = pdu[i];
			}
			++m_commEventCounter; // increment for each successful message completion
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		case RETURN_BUS_MESSAGE_COUNT:
		{
			std::cout << "Return bus message count " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			if (0x0000 != requestDataField)
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
// get bus message count
			m_rspPdu[0] = DIAGNOSTICS;
			m_rspPdu[1] = (RETURN_BUS_MESSAGE_COUNT & 0xff00) >> 8;
			m_rspPdu[2] = (RETURN_BUS_MESSAGE_COUNT & 0xff);
			m_rspPdu[3] = (m_busMessageCounter & 0xff00) >> 8;
			m_rspPdu[4] = (m_busMessageCounter & 0xff);

			++m_commEventCounter; // increment for each successful message completion
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		case RETURN_BUS_COMMUNICATION_ERROR_COUNT:
		{
			std::cout << "Return bus communication error count " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			if (0x0000 != requestDataField)
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
// get bus communication error count
			m_rspPdu[0] = DIAGNOSTICS;
			m_rspPdu[1] = (RETURN_BUS_COMMUNICATION_ERROR_COUNT & 0xff00) >> 8;
			m_rspPdu[2] = (RETURN_BUS_COMMUNICATION_ERROR_COUNT & 0xff);
			m_rspPdu[3] = (m_busCommunicationErrorCounter & 0xff00) >> 8;
			m_rspPdu[4] = (m_busCommunicationErrorCounter & 0xff);
			++m_commEventCounter; // increment for each successful message completion
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		case RETURN_BUS_EXCEPTION_ERROR_COUNT:
		{
			std::cout << "Return bus exception error count " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			if (0x0000 != requestDataField)
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
// get bus exception error count
			m_rspPdu[0] = DIAGNOSTICS;
			m_rspPdu[1] = (RETURN_BUS_EXCEPTION_ERROR_COUNT & 0xff00) >> 8;
			m_rspPdu[2] = (RETURN_BUS_EXCEPTION_ERROR_COUNT & 0xff);
			m_rspPdu[3] = (m_busExceptionErrorCounter & 0xff00) >> 8;
			m_rspPdu[4] = (m_busExceptionErrorCounter & 0xff);
			++m_commEventCounter; // increment for each successful message completion
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		case RETURN_SLAVE_MESSAGE_COUNT:
		{
			std::cout << "Return slave message count " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			if (0x0000 != requestDataField)
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
// get slave message count
			m_rspPdu[0] = DIAGNOSTICS;
			m_rspPdu[1] = (RETURN_SLAVE_MESSAGE_COUNT & 0xff00) >> 8;
			m_rspPdu[2] = (RETURN_SLAVE_MESSAGE_COUNT & 0xff);
			m_rspPdu[3] = (m_slaveMessageCounter & 0xff00) >> 8;
			m_rspPdu[4] = (m_slaveMessageCounter & 0xff);
			++m_commEventCounter; // increment for each successful message completion
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		case RETURN_SLAVE_NO_RESPONSE_COUNT:
		{
			std::cout << "Return slave no response count " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			if (0x0000 != requestDataField)
			{
				// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT |CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
// get slave no response count
			m_rspPdu[0] = DIAGNOSTICS;
			m_rspPdu[1] = (RETURN_SLAVE_NO_RESPONSE_COUNT & 0xff00) >> 8;
			m_rspPdu[2] = (RETURN_SLAVE_NO_RESPONSE_COUNT & 0xff);
			m_rspPdu[3] = (m_slaveNoResponseCounter & 0xff00) >> 8;
			m_rspPdu[4] = (m_slaveNoResponseCounter & 0xff);
			++m_commEventCounter; // increment for each successful message completion
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		case RETURN_SLAVE_NAK_COUNT:
		{
			std::cout << "Return slave NAK count " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			if (0x0000 != requestDataField)
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
// get slave NAL count
			m_rspPdu[0] = DIAGNOSTICS;
			m_rspPdu[1] = (RETURN_SLAVE_NAK_COUNT & 0xff00) >> 8;
			m_rspPdu[2] = (RETURN_SLAVE_NAK_COUNT & 0xff);
			m_rspPdu[3] = (m_slaveNAKCounter & 0xff00) >> 8;
			m_rspPdu[4] = (m_slaveNAKCounter & 0xff);
			++m_commEventCounter; // increment for each successful message completion
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		case RETURN_SLAVE_BUSY_COUNT:
		{
			std::cout << "Return slave busy count " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			if (0x0000 != requestDataField)
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
// get slave busy count
			m_rspPdu[0] = DIAGNOSTICS;
			m_rspPdu[1] = (RETURN_SLAVE_BUSY_COUNT & 0xff00) >> 8;
			m_rspPdu[2] = (RETURN_SLAVE_BUSY_COUNT & 0xff);
			m_rspPdu[3] = (m_slaveBusyCounter & 0xff00) >> 8;
			m_rspPdu[4] = (m_slaveBusyCounter & 0xff);
			++m_commEventCounter; // increment for each successful message completion
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		case RETURN_BUS_CHARACTER_OVERRUN_COUNT:
		{
			std::cout << "Return bus character overrun count " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			if (0x0000 != requestDataField)
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
// get bus character overrun count
			m_rspPdu[0] = DIAGNOSTICS;
			m_rspPdu[1] = (RETURN_BUS_CHARACTER_OVERRUN_COUNT & 0xff00) >> 8;

			m_rspPdu[2] = (RETURN_BUS_CHARACTER_OVERRUN_COUNT & 0xff);
			m_rspPdu[3] = (m_busCharacterOverrunCounter & 0xff00) >> 8;
			m_rspPdu[4] = (m_busCharacterOverrunCounter & 0xff);
			++m_commEventCounter; // increment for each successful message completion
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		case CLEAR_OVERRUN_COUNTER_AND_FLAG:
		{
			std::cout << "Clear overrun counter and flag " << std::endl;
			uint16_t requestDataField;
			requestDataField = (pdu[3] << 8) + pdu[4];
			if (0x0000 != requestDataField)
			{
// incorrect data value
				m_rspPdu[0] = DIAGNOSTICS + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
// clears the overrun error counter
			m_busCharacterOverrunCounter = 0;
// echo request data
			m_rspPdu[0] = DIAGNOSTICS;
			m_rspPdu[1] = (CLEAR_OVERRUN_COUNTER_AND_FLAG & 0xff00) >> 8;
			m_rspPdu[2] = (CLEAR_OVERRUN_COUNTER_AND_FLAG & 0xff);
			m_rspPdu[3] = 0;
			m_rspPdu[4] = 0;

			++m_commEventCounter; // increment for each successful message completion
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		default:
		{
			std::cout << "error: undefined sub-function code " << std::endl;
			m_rspPdu[0] = DIAGNOSTICS + 0x80;
			m_rspPdu[1] = 0x01; // exception code 0x01, sub-function code not supported
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
			return m_rspPdu;
		}
		}
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = DIAGNOSTICS + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
}

uint8_t*
ModbusMgr::GetCommEventCounter ()
{
// read exception status
	try
	{
		m_rspPdu[0] = GET_COMM_EVENT_COUNTER;
		m_rspPdu[1] = 0;
		m_rspPdu[2] = 0;
		m_rspPdu[3] = (m_commEventCounter & 0xff00) >> 8;
		m_rspPdu[4] = (m_commEventCounter & 0xff);
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
		return m_rspPdu;
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = GET_COMM_EVENT_COUNTER + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
}
uint8_t*
ModbusMgr::GetCommEventLog ()
{
// read exception status
	try
	{
		m_rspPdu[0] = GET_COMM_EVENT_LOG;
		m_rspPdu[1] = 6 + m_EventCounter; // byte count
		m_rspPdu[2] = 0;
		m_rspPdu[3] = 0;
		m_rspPdu[4] = (m_commEventCounter & 0xff00) >> 8;
		m_rspPdu[5] = (m_commEventCounter & 0xff);
		m_rspPdu[6] = (m_busMessageCounter & 0xff00) >> 8;
		m_rspPdu[7] = (m_busMessageCounter & 0xff);
// copy the events in the response
		for (uint16_t i = 0; i < m_EventCounter; ++i)
		{
			m_rspPdu[8+i] = m_events[i];
		}
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
		return m_rspPdu;
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = GET_COMM_EVENT_LOG + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
}
uint8_t*
ModbusMgr::WriteMultipleCoils (uint16_t startAdr, uint16_t quantityOfOutputs, uint8_t byteCount,
                               uint8_t * pdu)
{
	std::cout << "startAdr: " << startAdr << std::endl;
	std::cout << "quantityOfOutputs: " << quantityOfOutputs << std::endl;
	std::cout << "byteCount: " << (byteCount + 0) << std::endl;
	if ((0x0001 > quantityOfOutputs) || (0x07B0 < quantityOfOutputs))
	{
		m_rspPdu[0] = WRITE_MULTIPLE_COILS + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT |READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	uint8_t byteCountTemp = quantityOfOutputs / 8;
	if (0 != (quantityOfOutputs % 8))
	{
		++byteCountTemp;
	}
	if (byteCountTemp != byteCount)
	{
		m_rspPdu[0] = WRITE_MULTIPLE_COILS + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if (0xFFFF < startAdr + quantityOfOutputs)
	{
		m_rspPdu[0] = WRITE_MULTIPLE_COILS + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog (READ_EXCEPTION_SENT | REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
// write multiple coils
	try
	{
		uint8_t bitMask = 0x01;
		uint16_t pduRequestOutputByteIndex = 6;
		for (uint16_t i = 0, coilIndex = startAdr; i < quantityOfOutputs; ++i, ++coilIndex)
		{
			if (pdu[pduRequestOutputByteIndex] & bitMask)
			{
// turn on the coil
				m_coilsStatus[coilIndex] = 1; // LSb: 0 = OFF and 1 = ON
			}
			else
			{
// turn off the coil
				m_coilsStatus[coilIndex] = 0; // LSb: 0 = OFF and 1 = ON
			}
// shift the bit mask
			bitMask <<= 1;
			if (0 == bitMask)
			{
				bitMask = 0x1;
				++pduRequestOutputByteIndex;
			}
		}
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = WRITE_MULTIPLE_COILS + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
	m_rspPdu[0] = WRITE_MULTIPLE_COILS;
	m_rspPdu[1] = (startAdr & 0xff00) >> 8;
	m_rspPdu[2] = (startAdr & 0xff);
	m_rspPdu[3] = (quantityOfOutputs & 0xff00) >> 8;
	m_rspPdu[4] = (quantityOfOutputs & 0xff);
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}
uint8_t*
ModbusMgr::WriteMultipleRegisters (uint16_t startAdr, uint16_t quantityOfRegisters, uint8_t
                                   byteCount, uint8_t * pdu)
{
	std::cout << "startAdr: " << startAdr << std::endl;
	std::cout << "quantityOfRegisters: " << quantityOfRegisters << std::endl;
	std::cout << "byteCount: " << (byteCount + 0) << std::endl;
	if ((0x0001 > quantityOfRegisters) || (0x007B < quantityOfRegisters))
	{
		m_rspPdu[0] = WRITE_MULTIPLE_REGISTERS + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if ((quantityOfRegisters * 2) != byteCount)
	{
		m_rspPdu[0] = WRITE_MULTIPLE_REGISTERS + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if (0xFFFF < startAdr + quantityOfRegisters)
	{
		m_rspPdu[0] = WRITE_MULTIPLE_REGISTERS + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
// write multiple registers
	try
	{
		uint16_t pduRequestOutputByteIndex = 6;
		for (uint16_t i = 0, registerIndex = startAdr; i < quantityOfRegisters; ++i, ++registerIndex)
		{
			m_holdingRegisters[registerIndex] = (pdu[pduRequestOutputByteIndex] << 8) + pdu[pduRequestOutputByteIndex + 1];
			pduRequestOutputByteIndex += 2;
		}
	}

	catch (...)
	{
// exception
		m_rspPdu[0] = WRITE_MULTIPLE_REGISTERS + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
	m_rspPdu[0] = WRITE_MULTIPLE_REGISTERS;
	m_rspPdu[1] = (startAdr & 0xff00) >> 8;
	m_rspPdu[2] = (startAdr & 0xff);
	m_rspPdu[3] = (quantityOfRegisters & 0xff00) >> 8;
	m_rspPdu[4] = (quantityOfRegisters & 0xff);
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}
uint8_t*
ModbusMgr::ReportSlaveID ()
{
// report slave id
	try
	{
		m_rspPdu[0] = REPORT_SLAVE_ID;
		m_rspPdu[1] = 3; // byte count
		m_rspPdu[2] = 0; // slave id
		m_rspPdu[3] = 0xFF; // run indicator status. 0x00 = OFF, 0xFF = ON
		m_rspPdu[4] = 0; // additional information
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = REPORT_SLAVE_ID + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}
uint8_t*
ModbusMgr::ReadFileRecord (uint8_t byteCount, uint8_t * pdu)
{
	std::cout << "byteCount: " << (byteCount + 0) << std::endl;
	if ((0x0007 > byteCount) || (0x00F5 < byteCount))
	{
		m_rspPdu[0] = READ_FILE_RECORD + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	uint32_t pduRequestInputByteIndex = 2;
	uint32_t pduRequestOutputByteIndex = 2;
	uint32_t subRequestNumber = 1;
	for (uint16_t i = 0; i < byteCount; i+=7)
	{
		uint8_t referenceType = pdu[pduRequestInputByteIndex++];
		uint16_t fileNumber = (pdu[pduRequestInputByteIndex] << 8) + pdu[pduRequestInputByteIndex + 1]; // one based
		pduRequestInputByteIndex += 2;
		uint16_t recordNumber = (pdu[pduRequestInputByteIndex] << 8) + pdu[pduRequestInputByteIndex + 1]; // zero based
		pduRequestInputByteIndex += 2;
		uint16_t recordLength = (pdu[pduRequestInputByteIndex] << 8) + pdu[pduRequestInputByteIndex + 1];
		pduRequestInputByteIndex += 2;

		std::cout << "Sub-Req: " << (subRequestNumber + 0) << std::endl;
		std::cout << "referenceType: " << (referenceType + 0) << std::endl;
		std::cout << "fileNumber: " << (fileNumber + 0) << std::endl; // one based
		std::cout << "recordNumber: " << (recordNumber + 0) << std::endl; // zero based
		std::cout << "recordLength: " << (recordLength + 0) << std::endl;
		++subRequestNumber;
		if ((0x06 != referenceType) ||
		    (0x0001 > fileNumber || MAX_FILE_NUMBER < fileNumber) ||
		    (0x0000 > recordNumber || 0x270F < recordNumber) ||
		    (0x270f < recordNumber + recordLength))
		{
			m_rspPdu[0] = READ_FILE_RECORD + 0x80;
			m_rspPdu[1] = 0x02; // exception code 0x02
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
			return m_rspPdu;
		}
// read file record
		try
		{
			m_rspPdu[pduRequestOutputByteIndex++] = (recordLength << 1) + 1; // Sub-Req. x, File Resp.length
			m_rspPdu[pduRequestOutputByteIndex++] = 6; // Sub-Req. x, Reference Type
			for (uint16_t iRecCount = 0; iRecCount < recordLength; ++iRecCount)
			{
				m_rspPdu[pduRequestOutputByteIndex++] = (m_fileRecord[fileNumber-1][recordNumber+iRecCount] & 0xff00) >> 8;
				m_rspPdu[pduRequestOutputByteIndex++] = (m_fileRecord[fileNumber-1][recordNumber+iRecCount] & 0xff);
			}
		}
		catch (...)
		{
// exception
			m_rspPdu[0] = READ_FILE_RECORD + 0x80;
			m_rspPdu[1] = 0x04; // exception code 0x04
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
			return m_rspPdu;
		}
	}
	m_rspPdu[0] = READ_FILE_RECORD;
	m_rspPdu[1] = pduRequestOutputByteIndex - 2; // data length
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}

uint8_t*
ModbusMgr::WriteFileRecord (uint8_t requestDataLength, uint8_t * pdu)
{
	std::cout << "requestDataLength: " << (requestDataLength + 0) << std::endl;
	if ((0x0007 > requestDataLength) || (0x00F5 < requestDataLength))
	{
		m_rspPdu[0] = WRITE_FILE_RECORD + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	uint8_t pduRequestInputByteIndex = 2;
	uint32_t subRequestNumber = 1;
	
	while (pduRequestInputByteIndex <= (requestDataLength + 1))
	{
		uint8_t referenceType = pdu[pduRequestInputByteIndex++];
		uint16_t fileNumber = (pdu[pduRequestInputByteIndex] << 8) + pdu[pduRequestInputByteIndex + 1]; // one based
		pduRequestInputByteIndex += 2;
		uint16_t recordNumber = (pdu[pduRequestInputByteIndex] << 8) + pdu[pduRequestInputByteIndex + 1]; // zero based
		pduRequestInputByteIndex += 2;
		uint16_t recordLength = (pdu[pduRequestInputByteIndex] << 8) + pdu[pduRequestInputByteIndex + 1];
		pduRequestInputByteIndex += 2;

		std::cout << "Sub-Req: " << (subRequestNumber + 0) << std::endl;
		std::cout << "referenceType: " << (referenceType + 0) << std::endl;
		std::cout << "fileNumber: " << (fileNumber + 0) << std::endl; // one based
		std::cout << "recordNumber: " << (recordNumber + 0) << std::endl; // zero based
		std::cout << "recordLength: " << (recordLength + 0) << std::endl;

		++subRequestNumber;

		if ((0x06 != referenceType) ||
		    (0x0001 > fileNumber || MAX_FILE_NUMBER < fileNumber) ||
		    (0x0000 > recordNumber || 0x270F < recordNumber) ||
		    (0x270f < recordNumber + recordLength))
		{
			m_rspPdu[0] = WRITE_FILE_RECORD + 0x80;
			m_rspPdu[1] = 0x02; // exception code 0x02
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT |READ_EXCEPTION_SENT);
			return m_rspPdu;
		}
// write file record
		try
		{
			for (uint16_t iRecCount = 0; iRecCount < recordLength; ++iRecCount)
			{
				uint8_t regDataHigh = pdu[pduRequestInputByteIndex++];
				uint8_t regDataLow = pdu[pduRequestInputByteIndex++];
				m_fileRecord[fileNumber-1][recordNumber+iRecCount] = ((regDataHigh << 8) & 0xff00) + regDataLow;
			}
		}
		catch (...)
		{
// exception
			m_rspPdu[0] = WRITE_FILE_RECORD + 0x80;
			m_rspPdu[1] = 0x04; // exception code 0x04
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
			return m_rspPdu;
		}
	}
// return sucessful response, which is an echo of the request
	for (uint16_t i = 0; i < (requestDataLength + 2); ++i)
	{
		m_rspPdu[i] = pdu[i];
	}
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT |CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}
uint8_t*
ModbusMgr::MaskWriteRegister (uint16_t referenceAdr, uint16_t and_mask, uint16_t or_mask)
{
	std::cout << "referenceAdr: " << referenceAdr << std::endl;
	std::cout << "and_mask: " << and_mask << std::endl;
	std::cout << "or_mask: " << or_mask << std::endl;
	if ((0x0000 > referenceAdr) || (0xFFFF < referenceAdr)) // added for completeness
	{
		m_rspPdu[0] = MASK_WRITE_REGISTER + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if ((0x0000 > and_mask) || (0xFFFF < and_mask)) // added for completeness
	{
		m_rspPdu[0] = MASK_WRITE_REGISTER + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (READ_EXCEPTION_SENT | REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if ((0x0000 > or_mask) || (0xFFFF < or_mask)) // added for completeness
	{
		m_rspPdu[0] = MASK_WRITE_REGISTER + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
// mask write register
	try
	{
		uint16_t result = (m_holdingRegisters[referenceAdr] and and_mask) or (or_mask and (not
		                                                                                   and_mask));
		m_holdingRegisters[referenceAdr] = result;
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = MASK_WRITE_REGISTER + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT |SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}

	// successful response is an echo of the request
	m_rspPdu[0] = MASK_WRITE_REGISTER;
	m_rspPdu[1] = (referenceAdr & 0xff00) >> 8;
	m_rspPdu[2] = (referenceAdr & 0xff);
	m_rspPdu[3] = (and_mask & 0xff00) >> 8;
	m_rspPdu[4] = (and_mask & 0xff);
	m_rspPdu[5] = (or_mask & 0xff00) >> 8;
	m_rspPdu[6] = (or_mask & 0xff);
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}
uint8_t*
ModbusMgr::ReadWriteMultipleRegisters (uint16_t readStartAdr, uint16_t quantityToRead,
                                       uint16_t writeStartAdr, uint16_t quantityToWrite, uint8_t writeByteCount, uint8_t * pdu)
{
	std::cout << "readStartAdr: " << readStartAdr << std::endl;
	std::cout << "quantityToRead: " << quantityToRead << std::endl;
	std::cout << "writeStartAdr: " << writeStartAdr << std::endl;
	std::cout << "quantityToWrite: " << quantityToWrite << std::endl;
	std::cout << "writeByteCount: " << (writeByteCount + 0) << std::endl;
	if ((0x0001 > quantityToRead) || (0x007D < quantityToRead))
	{
		m_rspPdu[0] = READ_WRITE_MULTIPLE_REGISTERS + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}

	if ((0x0001 > quantityToWrite) || (0x007D < quantityToWrite))
	{
		m_rspPdu[0] = READ_WRITE_MULTIPLE_REGISTERS + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if (writeByteCount != (2 * quantityToWrite))
	{
		m_rspPdu[0] = READ_WRITE_MULTIPLE_REGISTERS + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT |READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if ((0x0000 > readStartAdr) || (0xFFFF < readStartAdr)) // added for completeness
	{
		m_rspPdu[0] = READ_WRITE_MULTIPLE_REGISTERS + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}

	if (0xFFFF < readStartAdr + quantityToRead)
	{
		m_rspPdu[0] = READ_WRITE_MULTIPLE_REGISTERS + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if ((0x0000 > writeStartAdr) || (0xFFFF < writeStartAdr)) // added for completeness
	{
		m_rspPdu[0] = READ_WRITE_MULTIPLE_REGISTERS + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if (0xFFFF < writeStartAdr + quantityToWrite)
	{
		m_rspPdu[0] = READ_WRITE_MULTIPLE_REGISTERS + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
// read/write multiple registers
	try
	{
// write multiple registers
		uint16_t pduRequestInputByteIndex = 10;
		for (uint16_t i = 0, registerIndex = writeStartAdr; i < quantityToWrite; ++i, ++registerIndex)
		{
			m_holdingRegisters[registerIndex] = (pdu[pduRequestInputByteIndex] << 8) + pdu[pduRequestInputByteIndex + 1];
			pduRequestInputByteIndex += 2;
		}
// read multiple registers
		for (uint16_t i = readStartAdr, rspIndex = 2; i < (readStartAdr + quantityToRead); ++i, rspIndex+=2)
		{
			m_rspPdu[rspIndex] = (m_holdingRegisters[i] & 0xff00) >> 8;
			m_rspPdu[rspIndex+1] = (m_holdingRegisters[i] & 0xff);
		}
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = READ_WRITE_MULTIPLE_REGISTERS + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04

		(m_listenOnlyMode) ?
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT |SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog
		        (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
// successful response is an echo of the request
	m_rspPdu[0] = READ_WRITE_MULTIPLE_REGISTERS;
	m_rspPdu[1] = quantityToRead * 2;
	++m_commEventCounter; // increment for each successful message completion
	(m_listenOnlyMode) ? PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) : PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}

uint8_t*
ModbusMgr::ReadFifoQueue (uint16_t fifoPointerAdr)
{
	std::cout << "fifoPointerAdr: " << fifoPointerAdr << std::endl;
	if ((0x0000 > fifoPointerAdr) || (0xFFFF < fifoPointerAdr)) // added for completeness
	{
		m_rspPdu[0] = READ_FIFO_QUEUE + 0x80;
		m_rspPdu[1] = 0x02; // exception code 0x02
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}
	if (MAX_FIFO_COUNT < m_fifoCount)
	{
		m_rspPdu[0] = READ_FIFO_QUEUE + 0x80;
		m_rspPdu[1] = 0x03; // exception code 0x03
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
		return m_rspPdu;
	}

	// read fifo queue
	uint32_t pduRequestOutputByteIndex = 3;
	try
	{
		m_rspPdu[pduRequestOutputByteIndex++] = (m_fifoCount & 0xff00) >> 8;
		m_rspPdu[pduRequestOutputByteIndex++] = (m_fifoCount & 0xff);
		for (uint16_t i = fifoPointerAdr; i < (fifoPointerAdr + m_fifoCount) && i < MAX_FIFO_QUEUE_SIZE; ++i)
		{
			m_rspPdu[pduRequestOutputByteIndex++] = (m_fifo[i] & 0xff00) >> 8;
			m_rspPdu[pduRequestOutputByteIndex++] = (m_fifo[i] & 0xff);
		}
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = READ_FIFO_QUEUE + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04
		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}

	m_rspPdu[0] = READ_FIFO_QUEUE;
	m_rspPdu[1] = ((pduRequestOutputByteIndex - 3) & 0xff00) >> 8; // byte count high
	m_rspPdu[2] = ((pduRequestOutputByteIndex - 3) & 0xff); // byte count low

	++m_commEventCounter; // increment for each successful message completion

	(m_listenOnlyMode) ?
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
	PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
	return m_rspPdu;
}

uint8_t*
ModbusMgr::encapsulatedInterfaceTransport (uint8_t meiType, uint8_t * pdu)
{
	std::cout << "meiType: 0x" << std::hex << (meiType + 0) << std::dec << std::endl;
	try
	{
		switch (meiType)
		{
		case CANOPEN_GENERAL_REFERENCE_REQUEST_AND_RESPONSE_PDU:
		{
			std::cout << "CAN open General Reference Request and Response PDU " << std::endl;
// return sucessful response, which is an echo of the request
			for (uint16_t i = 0; i < MAX_MODBUS_PDU; ++i)
			{
				m_rspPdu[i] = pdu[i];
			}

			++m_commEventCounter; // increment for each successful message completion

			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
			return m_rspPdu;
		}
		case READ_DEVICE_IDENTIFICATION:
		{
			std::cout << "Read Device Identification " << std::endl;
			uint8_t readDeviceIdCode;
			uint8_t objectId;
			readDeviceIdCode = pdu[2];
			objectId = pdu[3];
			std::cout << "readDeviceIdCode " << (readDeviceIdCode + 0) << std::endl;
			std::cout << "objectId " << (objectId + 0) << std::endl;
			switch (readDeviceIdCode)
			{
			case READ_BASIC_DEVICE_ID:
			{
				uint32_t iIndex = 0;
				m_rspPdu[iIndex++] = ENCAPSULATED_INTERFACE_TRANSPORT;
				m_rspPdu[iIndex++] = READ_DEVICE_IDENTIFICATION;
				m_rspPdu[iIndex++] = READ_BASIC_DEVICE_ID;
				m_rspPdu[iIndex++] = CL_BASIC_ID_STREAM_ACCESS_AND_INDIVIDUAL_ACCESS;
				m_rspPdu[iIndex++] = 0x00; // more follows. 0x00 no more objects follows, 0xFF follows
				m_rspPdu[iIndex++] = 0x00; // Next Object ID
				m_rspPdu[iIndex++] = 0x03; // Number of objects
				m_rspPdu[iIndex++] = VENDOR_NAME_ID;
				m_rspPdu[iIndex++] = VENDOR_NAME.length(); // Object length
				for (uint32_t i = 0; i < VENDOR_NAME.length(); ++i)
				{
					m_rspPdu[iIndex++] = VENDOR_NAME.at(i);
				}
				m_rspPdu[iIndex++] = PRODUCT_CODE_ID;
				m_rspPdu[iIndex++] = PRODUCT_CODE.length(); // Object length
				for (uint32_t i = 0; i < PRODUCT_CODE.length(); ++i)
				{
					m_rspPdu[iIndex++] = PRODUCT_CODE.at(i);
				}
				m_rspPdu[iIndex++] = MAJOR_MINOR_REVISION_ID;
				m_rspPdu[iIndex++] = MAJOR_MINOR_REVISION.length(); // Object length
				for (uint32_t i = 0; i < MAJOR_MINOR_REVISION.length(); ++i)
				{
					m_rspPdu[iIndex++] = MAJOR_MINOR_REVISION.at(i);
				}

				++m_commEventCounter; // increment for each successful message completion

				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
				return m_rspPdu;
			}
			case READ_REGULAR_DEVICE_ID:
			{
				m_rspPdu[0] = ENCAPSULATED_INTERFACE_TRANSPORT;
				m_rspPdu[1] = READ_DEVICE_IDENTIFICATION;
				m_rspPdu[2] = READ_REGULAR_DEVICE_ID;
				m_rspPdu[3] = CL_BASIC_ID_STREAM_ACCESS_AND_INDIVIDUAL_ACCESS;
				m_rspPdu[4] = 0x00; // more follows. 0x00 no more objects follows, 0xFF follows
				m_rspPdu[5] = 0x00; // Next Object ID
				m_rspPdu[6] = 0x00; // Number of objects

				++m_commEventCounter; // increment for each successful message completion

				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
				return m_rspPdu;
			}
			case READ_EXTENDED_DEVICE_ID:
			{
				m_rspPdu[0] = ENCAPSULATED_INTERFACE_TRANSPORT;
				m_rspPdu[1] = READ_DEVICE_IDENTIFICATION;
				m_rspPdu[2] = READ_EXTENDED_DEVICE_ID;
				m_rspPdu[3] = CL_BASIC_ID_STREAM_ACCESS_AND_INDIVIDUAL_ACCESS;
				m_rspPdu[4] = 0x00; // more follows. 0x00 no more objects follows, 0xFF follows
				m_rspPdu[5] = 0x00; // Next Object ID
				m_rspPdu[6] = 0x00; // Number of objects
				++m_commEventCounter; // increment for each successful message completion
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
				return m_rspPdu;
			}
			case READ_SPECIFIC_DEVICE_ID: // individual access
			{
// just object id of VENDOR_NAME_ID, PRODUCT_CODE_ID, and
// MAJOR_MINOR_REVISION_ID are supported
				if (VENDOR_NAME_ID == objectId)
				{
					m_rspPdu[0] = ENCAPSULATED_INTERFACE_TRANSPORT;
					m_rspPdu[1] = READ_DEVICE_IDENTIFICATION;
					m_rspPdu[2] = READ_SPECIFIC_DEVICE_ID;
					m_rspPdu[3] = CL_BASIC_ID_STREAM_ACCESS_AND_INDIVIDUAL_ACCESS;
					m_rspPdu[4] = 0x00; // more follows. 0x00 no more objects follows, 0xFF follows
					m_rspPdu[5] = 0x00; // Next Object ID
					m_rspPdu[6] = 0x01; // Number of objects
					m_rspPdu[7] = VENDOR_NAME_ID;
					m_rspPdu[8] = VENDOR_NAME.length(); // Object length
					for (uint32_t i = 0, iIndex = 9; i < VENDOR_NAME.length(); ++i, ++iIndex)
					{
						m_rspPdu[iIndex] = VENDOR_NAME.at(i);
					}
					++m_commEventCounter; // increment for each successful message completion
					(m_listenOnlyMode) ?
					PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
					PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
					return m_rspPdu;
				}
				else if (PRODUCT_CODE_ID == objectId)
				{
					m_rspPdu[0] = ENCAPSULATED_INTERFACE_TRANSPORT;
					m_rspPdu[1] = READ_DEVICE_IDENTIFICATION;
					m_rspPdu[2] = READ_SPECIFIC_DEVICE_ID;
					m_rspPdu[3] = CL_BASIC_ID_STREAM_ACCESS_AND_INDIVIDUAL_ACCESS;
					m_rspPdu[4] = 0x00; // more follows. 0x00 no more objects follows, 0xFF follows
					m_rspPdu[5] = 0x00; // Next Object ID
					m_rspPdu[6] = 0x01; // Number of objects
					m_rspPdu[7] = PRODUCT_CODE_ID;
					m_rspPdu[8] = PRODUCT_CODE.length(); // Object length
					for (uint32_t i = 0, iIndex = 9; i < PRODUCT_CODE.length(); ++i, ++iIndex)
					{
						m_rspPdu[iIndex] = PRODUCT_CODE.at(i);
					}
					++m_commEventCounter; // increment for each successful message completion
					(m_listenOnlyMode) ?
					PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
					PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
					return m_rspPdu;
				}
				else if (MAJOR_MINOR_REVISION_ID == objectId)
				{
					m_rspPdu[0] = ENCAPSULATED_INTERFACE_TRANSPORT;
					m_rspPdu[1] = READ_DEVICE_IDENTIFICATION;
					m_rspPdu[2] = READ_SPECIFIC_DEVICE_ID;
					m_rspPdu[3] = CL_BASIC_ID_STREAM_ACCESS_AND_INDIVIDUAL_ACCESS;
					m_rspPdu[4] = 0x00; // more follows. 0x00 no more objects follows, 0xFF follows
					m_rspPdu[5] = 0x00; // Next Object ID
					m_rspPdu[6] = 0x01; // Number of objects
					m_rspPdu[7] = MAJOR_MINOR_REVISION_ID;
					m_rspPdu[8] = MAJOR_MINOR_REVISION.length(); // Object length
					for (uint32_t i = 0, iIndex = 9; i < MAJOR_MINOR_REVISION.length(); ++i,
					     ++iIndex)
					{
						m_rspPdu[iIndex] = MAJOR_MINOR_REVISION.at(i);
					}
					++m_commEventCounter; // increment for each successful message completion
					(m_listenOnlyMode) ?
					PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
					PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT);
					return m_rspPdu;
				}
				else
				{
					m_rspPdu[0] = ENCAPSULATED_INTERFACE_TRANSPORT + 0x80;
					m_rspPdu[1] = 0x02; // exception code 0x02
					(m_listenOnlyMode) ?
					PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
					PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
					return m_rspPdu;
				}
			}
			default:
			{
				m_rspPdu[0] = ENCAPSULATED_INTERFACE_TRANSPORT + 0x80;
				m_rspPdu[1] = 0x03; // exception code 0x03
				(m_listenOnlyMode) ?
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
				PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
				return m_rspPdu;
			}
			}
		}
		default:
		{
			std::cout << "error: undefined MEI type " << std::endl;
			m_rspPdu[0] = ENCAPSULATED_INTERFACE_TRANSPORT + 0x80;
			m_rspPdu[1] = 0x01; // exception code 0x01, sub-function code not supported
			(m_listenOnlyMode) ?
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
			PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | READ_EXCEPTION_SENT);
			return m_rspPdu;
		}
		}
	}
	catch (...)
	{
// exception
		m_rspPdu[0] = ENCAPSULATED_INTERFACE_TRANSPORT + 0x80;
		m_rspPdu[1] = 0x04; // exception code 0x04

		(m_listenOnlyMode) ?
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT | CURRENTLY_IN_LISTEN_MODE_ONLY) :
		PushEventLog (REMOTE_DEVICE_MODBUS_SEND_EVENT | SLAVE_ABORT_EXCEPTION_SENT);
		return m_rspPdu;
	}
}
} // namespace ns3