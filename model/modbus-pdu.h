/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#ifndef MODBUS_PDU_H
#define MODBUS_PDU_H
#include "ns3/header.h"
#include "ns3/nstime.h"
const uint32_t PDU_SIZE = 253;
namespace ns3 {
/**
 * \ingroup modbus
 * \class ModbusPdu
 * \brief Packet header for MODBUS application
 * The header is made of a pointer to uint8_t.
 */
class ModbusPdu : public Header
{
public:
ModbusPdu ();
virtual ~ModbusPdu ();
/**
 * \return the pdu
 */
uint8_t* GetPdu (void) const;
static TypeId GetTypeId (void);
private:
virtual TypeId GetInstanceTypeId (void) const;
virtual void Print (std::ostream &os) const;
virtual uint32_t GetSerializedSize (void) const;
virtual void Serialize (Buffer::Iterator start) const;
virtual uint32_t Deserialize (Buffer::Iterator start);
uint8_t *m_pdu;
};
} // namespace ns3
#endif /* MODBUS_PDU_H */