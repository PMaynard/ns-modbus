/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/nstime.h"
#include "ns3/inet-socket-address.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "ns3/packet-loss-counter.h"
#include "ns3/seq-ts-header.h"
#include "modbus-udp-server.h"
#include "modbus-pdu.h"
namespace ns3 {
NS_LOG_COMPONENT_DEFINE ("ModbusUdpServer");
NS_OBJECT_ENSURE_REGISTERED (ModbusUdpServer);
TypeId
ModbusUdpServer::GetTypeId (void)
{
	static TypeId tid = TypeId ("ns3::ModbusUdpServer")
	                    .SetParent<Application> ()
	                    .AddConstructor<ModbusUdpServer> ()
	                    .AddAttribute ("Port", "Port on which we listen for incoming packets.",
	                                   UintegerValue (502),
	                                   MakeUintegerAccessor (&ModbusUdpServer::m_port),
	                                   MakeUintegerChecker<uint16_t> ())
	                    .AddAttribute ("PacketWindowSize", "The size of the window used to compute the packet loss. This value should be amultiple of 8.",
	                                   UintegerValue (32),
	                                   MakeUintegerAccessor (&ModbusUdpServer::GetPacketWindowSize,
	                                                         &ModbusUdpServer::SetPacketWindowSize),
	                                   MakeUintegerChecker<uint16_t> (8,256))
	;
	return tid;
}
ModbusUdpServer::ModbusUdpServer ()
	: m_lossCounter (0)
{
	NS_LOG_FUNCTION (this);
	m_received=0;
}
ModbusUdpServer::~ModbusUdpServer ()
{
	NS_LOG_FUNCTION (this);
}
uint16_t
ModbusUdpServer::GetPacketWindowSize () const
{
	return m_lossCounter.GetBitMapSize ();
}
void
ModbusUdpServer::SetPacketWindowSize (uint16_t size)
{
	m_lossCounter.SetBitMapSize (size);
}
uint32_t
ModbusUdpServer::GetLost (void) const
{
	return m_lossCounter.GetLost ();
}
uint32_t
ModbusUdpServer::GetReceived (void) const
{
	return m_received;
}
void
ModbusUdpServer::DoDispose (void)
{
	NS_LOG_FUNCTION (this);
	Application::DoDispose ();
}
void
ModbusUdpServer::StartApplication (void)
{
	NS_LOG_FUNCTION_NOARGS ();
	if (m_socket == 0)
	{
		TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
		m_socket = Socket::CreateSocket (GetNode (), tid);
		InetSocketAddress local = InetSocketAddress (Ipv4Address::GetAny (), m_port);
		m_socket->Bind (local);
	}
	m_socket->SetRecvCallback (MakeCallback (&ModbusUdpServer::HandleRead, this));
}
void
ModbusUdpServer::StopApplication ()
{
	NS_LOG_FUNCTION (this);
	if (m_socket != 0)
	{
		m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
	}
}
void
ModbusUdpServer::HandleRead (Ptr<Socket> socket)
{
	NS_LOG_FUNCTION (this << socket);
	Ptr<Packet> packet;
	Address from;
	while (packet = socket->RecvFrom (from))
	{
		if (InetSocketAddress::IsMatchingType (from))
		{
			if (packet->GetSize () > 0)
			{
				NS_LOG_INFO ("Parsing MODBUS UDP function code");
				uint32_t packetSize = packet->GetSize() - 12; // 8+4 : the size of the seqTs header
				ModbusPdu cModbusRequestPdu;
				packet->RemoveHeader (cModbusRequestPdu);
				uint8_t *pdu = cModbusRequestPdu.GetPdu ();
				uint8_t *rspPdu = m_cModbusMgr.DoMain(pdu);
				SeqTsHeader seqTs;
				packet->RemoveHeader (seqTs);
				uint32_t currentSequenceNumber = seqTs.GetSeq ();
				NS_LOG_INFO ("TraceDelay: RX " << packetSize <<
				             " bytes from "<< InetSocketAddress::ConvertFrom (from).GetIpv4 () <<
				             " Sequence Number: " << currentSequenceNumber <<
				             " Uid: " << packet->GetUid () <<
				             " TXtime: " << seqTs.GetTs () <<
				             " RXtime: " << Simulator::Now () <<
				             " Delay: " << Simulator::Now () - seqTs.GetTs ());
				m_lossCounter.NotifyReceived (currentSequenceNumber);
				m_received++;
				if (NULL != rspPdu)
				{
// send MODBUS UDP response back
					NS_LOG_INFO ("Sending MODBUS UDP response packet");
					packet->RemoveAllPacketTags ();
					packet->RemoveAllByteTags ();
					ModbusPdu cModbusResponsePdu;
					uint8_t *pduRspTemp = cModbusResponsePdu.GetPdu ();
					for (uint16_t i = 0; i < MAX_MODBUS_PDU; ++i)
					{
						pduRspTemp[i] = rspPdu[i];
					}
					packet->AddHeader (cModbusResponsePdu);
					socket->SendTo (packet, 0, from);
				}
			}
		}
	}
}
} // Namespace ns3

