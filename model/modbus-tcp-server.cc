/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/nstime.h"
#include "ns3/inet-socket-address.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "ns3/packet-loss-counter.h"
#include "ns3/seq-ts-header.h"
#include "ns3/modbus-tcp-server.h"
#include "ns3/modbus-pdu.h"

namespace ns3 {
NS_LOG_COMPONENT_DEFINE ("ModbusTcpServer");
NS_OBJECT_ENSURE_REGISTERED (ModbusTcpServer);

TypeId
ModbusTcpServer::GetTypeId (void)
{
	static TypeId tid = TypeId ("ns3::ModbusTcpServer")
	                    .SetParent<Application> ()
	                    .AddConstructor<ModbusTcpServer> ()
	                    .AddAttribute ("Port", "Port on which we listen for incoming packets.",
	                                   UintegerValue (502),
	                                   MakeUintegerAccessor (&ModbusTcpServer::m_port),
	                                   MakeUintegerChecker<uint16_t> ())
	                    .AddAttribute ("PacketWindowSize", "The size of the window used to compute the packet loss. This value should be a multiple of 8.",
	                                   UintegerValue (32),
	                                   MakeUintegerAccessor (&ModbusTcpServer::GetPacketWindowSize,
	                                                         &ModbusTcpServer::SetPacketWindowSize),
	                                   MakeUintegerChecker<uint16_t> (8,256))
	;
	return tid;
}
ModbusTcpServer::ModbusTcpServer ()
	: m_lossCounter (0)
{
	NS_LOG_FUNCTION (this);
	m_received=0;
}
ModbusTcpServer::~ModbusTcpServer ()
{
	NS_LOG_FUNCTION (this);
}
uint16_t
ModbusTcpServer::GetPacketWindowSize () const
{
	return m_lossCounter.GetBitMapSize ();
}
void
ModbusTcpServer::SetPacketWindowSize (uint16_t size)
{
	m_lossCounter.SetBitMapSize (size);
}
uint32_t
ModbusTcpServer::GetLost (void) const
{
	return m_lossCounter.GetLost ();
}
uint32_t
ModbusTcpServer::GetReceived (void) const
{
	return m_received;
}
Ptr<Socket>
ModbusTcpServer::GetListeningSocket (void) const
{
	NS_LOG_FUNCTION (this);
	return m_socket;
}
std::list<Ptr<Socket> >
ModbusTcpServer::GetAcceptedSockets (void) const
{
	NS_LOG_FUNCTION (this);
	return m_socketList;
}
void
ModbusTcpServer::DoDispose (void)
{
	NS_LOG_FUNCTION (this);
	m_socket = 0;
	m_socketList.clear ();
	Application::DoDispose ();
}
void
ModbusTcpServer::StartApplication (void)
{
	NS_LOG_FUNCTION_NOARGS ();
	if (m_socket == 0)
	{
		TypeId tid = TypeId::LookupByName ("ns3::TcpSocketFactory");
		m_socket = Socket::CreateSocket (GetNode (), tid);
		m_socket->SetAttribute ("DelAckCount", UintegerValue (1));
		InetSocketAddress local = InetSocketAddress (Ipv4Address::GetAny (), m_port);
		m_socket->Bind (local);
		// std::cout << "ModbusTcpServer::StartApplication Ipv4Address::GetAny Ipv4Address::GetAny () " << " m_port " << m_port << std::endl;
		//std::cout << "ModbusTcpServer::StartApplication local " << local << std::endl;
		m_socket->Listen ();
	}
	m_socket->SetRecvCallback (MakeCallback (&ModbusTcpServer::HandleRead, this));
	m_socket->SetAcceptCallback (
		MakeNullCallback<bool, Ptr<Socket>, const Address &> (),
		MakeCallback (&ModbusTcpServer::HandleAccept, this));
	m_socket->SetCloseCallbacks (
		MakeCallback (&ModbusTcpServer::HandlePeerClose, this),
		MakeCallback (&ModbusTcpServer::HandlePeerError, this));


}
void
ModbusTcpServer::StopApplication ()
{
	NS_LOG_FUNCTION (this);
/*
   if (m_socket != 0)
   {
   m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
   }
 */
	while (!m_socketList.empty ()) // these are accepted sockets, close them
	{
		Ptr<Socket> acceptedSocket = m_socketList.front ();
		m_socketList.pop_front ();
		acceptedSocket->Close ();
	}
	if (m_socket)
	{
		m_socket->Close ();
		m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
	}
}
void
ModbusTcpServer::HandleRead (Ptr<Socket> socket)
{
	NS_LOG_FUNCTION (this << socket);
	Ptr<Packet> packet;
	Address from;
	while (packet = socket->RecvFrom (from))
	{
		if (InetSocketAddress::IsMatchingType (from))
		{
			if (packet->GetSize () > 0)
			{
				NS_LOG_INFO ("Parsing MODBUS TCP function code");
				uint32_t packetSize = packet->GetSize() - 12; // 8+4 : the size of the seqTs header
				ModbusPdu cModbusRequestPdu;
				packet->RemoveHeader (cModbusRequestPdu);
				uint8_t *pdu = cModbusRequestPdu.GetPdu ();
				uint8_t *rspPdu = m_cModbusMgr.DoMain(pdu);
				SeqTsHeader seqTs;
				packet->RemoveHeader (seqTs);
				uint32_t currentSequenceNumber = seqTs.GetSeq ();
				NS_LOG_INFO ("TraceDelay: RX " << packetSize <<
				             " bytes from "<< InetSocketAddress::ConvertFrom (from).GetIpv4 () <<
				             " Sequence Number: " << currentSequenceNumber <<
				             " Uid: " << packet->GetUid () <<
				             " TXtime: " << seqTs.GetTs () <<
				             " RXtime: " << Simulator::Now () <<
				             " Delay: " << Simulator::Now () - seqTs.GetTs ());
				m_lossCounter.NotifyReceived (currentSequenceNumber);
				m_received++;
				if (NULL != rspPdu)
				{
// send MODBUS TCP response back
					NS_LOG_INFO ("Sending MODBUS TCP response packet");
					ModbusPdu cModbusResponsePdu;
					uint8_t *pduRspTemp = cModbusResponsePdu.GetPdu ();
					for (uint16_t i = 0; i < MAX_MODBUS_PDU; ++i)
					{
						pduRspTemp[i] = rspPdu[i];
					}
					Ptr<Packet> sendPacket = Create<Packet> (0);
					sendPacket->AddHeader (cModbusResponsePdu);
					socket->SendTo (sendPacket, 0, from);
				}
			}
		}
	}
}
void
ModbusTcpServer::HandlePeerClose (Ptr<Socket> socket)
{
	NS_LOG_INFO ("ModbusTcpServer, peerClose");
}
void
ModbusTcpServer::HandlePeerError (Ptr<Socket> socket)
{
	NS_LOG_INFO ("ModbusTcpServer, peerError");
}
void
ModbusTcpServer::HandleAccept (Ptr<Socket> s, const Address& from)
{
	NS_LOG_FUNCTION (this << s << from);
	s->SetRecvCallback (MakeCallback (&ModbusTcpServer::HandleRead, this));
	m_socketList.push_back (s);
}
} // Namespace ns3
