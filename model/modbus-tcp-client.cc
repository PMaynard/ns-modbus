/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/nstime.h"
#include "ns3/inet-socket-address.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "ns3/trace-source-accessor.h"
#include "modbus-tcp-client.h"
#include "ns3/seq-ts-header.h"
#include "modbus-pdu.h"
#include "modbus-mgr.h"
#include <stdlib.h>
#include <stdio.h>

namespace ns3 {
NS_LOG_COMPONENT_DEFINE ("ModbusTcpClient");
NS_OBJECT_ENSURE_REGISTERED (ModbusTcpClient);
TypeId
ModbusTcpClient::GetTypeId (void)
{
	static TypeId tid = TypeId ("ns3::ModbusTcpClient")
	                    .SetParent<Application> ()
	                    .AddConstructor<ModbusTcpClient> ()
	                    .AddAttribute ("MaxPackets",
	                                   "The maximum number of packets the application will send",
	                                   UintegerValue (100),
	                                   MakeUintegerAccessor (&ModbusTcpClient::m_count),
	                                   MakeUintegerChecker<uint32_t> ())
	                    .AddAttribute ("Interval",
	                                   "The time to wait between packets",
	                                   TimeValue (Seconds (1.0)),
	                                   MakeTimeAccessor (&ModbusTcpClient::m_interval),
	                                   MakeTimeChecker ())
	                    .AddAttribute ("RemoteAddress",
	                                   "The destination Ipv4Address of the outbound packets",
	                                   Ipv4AddressValue (),
	                                   MakeIpv4AddressAccessor (&ModbusTcpClient::m_peerAddress),
	                                   MakeIpv4AddressChecker ())
	                    .AddAttribute ("RemotePort", "The destination port of the outbound packets",
	                                   UintegerValue (502),
	                                   MakeUintegerAccessor (&ModbusTcpClient::m_peerPort),
	                                   MakeUintegerChecker<uint16_t> ())
	                    .AddAttribute ("PacketSize",
	                                   "Size of packets generated. The minimum packet size is 12 bytes which is the size of the header carrying the sequence number and the time stamp.",
	                                   UintegerValue (253),
	                                   MakeUintegerAccessor (&ModbusTcpClient::m_size),
	                                   MakeUintegerChecker<uint32_t> (12,1500))
	                    // TODO: Figure out the new way to AddTraceSource. (Also see UDP Client)
	                    // .AddTraceSource ("Tx", "A new packet is created and is sent",
	                    //                  MakeTraceSourceAccessor (&ModbusTcpClient::m_txTrace))
	;
	return tid;
}
ModbusTcpClient::ModbusTcpClient ()
{
	NS_LOG_FUNCTION_NOARGS ();
	m_sent = 0;
	m_socket = 0;
	m_sendEvent = EventId ();
	m_data = new uint8_t [MAX_MODBUS_PDU];
	m_dataSize = 0;
	m_count = 1;
	m_modbusRequests = new uint8_t *[MAX_NUM_MODBUS_TCP_REQUEST]; // rows
	for (uint32_t i = 0; i < MAX_NUM_MODBUS_TCP_REQUEST; ++i)
	{
		m_modbusRequests[i] = new uint8_t [MAX_MODBUS_PDU]; // cols
	}
	m_modbusRequestsInterval = new Time [MAX_NUM_MODBUS_TCP_REQUEST];
	m_modbusRequestsIndex = 0;
	m_modbusSendRequestsIndex = 0;
}
ModbusTcpClient::~ModbusTcpClient ()
{
	NS_LOG_FUNCTION_NOARGS ();
	m_socket = 0;
	delete [] m_data;
	m_data = 0;
	m_dataSize = 0;
	for (uint32_t i = 0; i < MAX_NUM_MODBUS_TCP_REQUEST; ++i)
	{
		delete [] m_modbusRequests[i];
	}
	delete [] m_modbusRequests;
	m_modbusRequests = 0;
	delete [] m_modbusRequestsInterval;
}
void
ModbusTcpClient::SetRemote (Ipv4Address ip, uint16_t port)
{
	m_peerAddress = ip;
	m_peerPort = port;
}
void
ModbusTcpClient::SetRequest (Time requestInterval, uint8_t *pdu)
{
	NS_LOG_INFO ("ModbusTcpClient::SetRequest ()");
	memcpy (m_modbusRequests[m_modbusRequestsIndex], pdu, MAX_MODBUS_PDU);
	m_modbusRequestsInterval[m_modbusRequestsIndex++] = requestInterval;
}
void
ModbusTcpClient::SetCount (uint32_t count)
{
	NS_LOG_INFO ("ModbusTcpClient::SetCount ()");
	m_count = count;
}
/*
   std::list<Ptr<Socket> >
   ModbusTcpClient::GetAcceptedSockets (void) const
   {
   NS_LOG_FUNCTION (this);
   return m_socketList;
   }
 */
void
ModbusTcpClient::DoDispose (void)
{
	NS_LOG_FUNCTION_NOARGS ();
//m_socket = 0;
//m_socketList.clear ();
	Application::DoDispose ();
}
void
ModbusTcpClient::StartApplication (void)
{
	NS_LOG_FUNCTION_NOARGS ();
//std::cout << "ModbusTcpClient::StartApplication " << std::endl;
	if (m_socket == 0)
	{
		TypeId tid = TypeId::LookupByName ("ns3::TcpSocketFactory");
		m_socket = Socket::CreateSocket (GetNode (), tid);
		m_socket->SetAttribute ("DelAckCount", UintegerValue (1));
		m_socket->Bind ();
		m_socket->Connect (InetSocketAddress (m_peerAddress, m_peerPort));
		m_socket->Listen ();
	}
	m_socket->SetRecvCallback (MakeCallback (&ModbusTcpClient::HandleRead, this));
	m_socket->SetAcceptCallback (
		MakeNullCallback<bool, Ptr<Socket>, const Address &> (),
		MakeCallback (&ModbusTcpClient::HandleAccept, this));
/*
   m_socket->SetCloseCallbacks (
   MakeCallback (&ModbusTcpClient::HandlePeerClose, this),
   MakeCallback (&ModbusTcpClient::HandlePeerError, this));
 */
	if (m_modbusSendRequestsIndex < m_modbusRequestsIndex)
	{
		m_sendEvent
			=
				Simulator::Schedule
				        (m_modbusRequestsInterval[0],
				        &ModbusTcpClient::Send, this);
	}
}
void
ModbusTcpClient::StopApplication ()
{
	NS_LOG_FUNCTION_NOARGS ();
	if (m_socket != 0)
	{
		m_socket->Close ();
		m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
		m_socket = 0;
	}
	Simulator::Cancel (m_sendEvent);
}
void
ModbusTcpClient::Send (void)
{
	NS_LOG_INFO ("ModbusTcpClient::Send ()");
	NS_LOG_FUNCTION_NOARGS ();
	NS_ASSERT (m_sendEvent.IsExpired ());
	SeqTsHeader seqTs;
	seqTs.SetSeq (m_sent);
	m_size = 0;
	Ptr<Packet> p = Create<Packet> (m_size);
// call to the trace sinks before the packet is actually sent,
// so that tags added to the packet can be sent as well
	m_txTrace (p);
	p->AddHeader (seqTs);
	ModbusPdu cModbusRequestPdu;
	uint8_t *pdu = cModbusRequestPdu.GetPdu ();
	memcpy (pdu, m_modbusRequests[m_modbusSendRequestsIndex++], MAX_MODBUS_PDU);
	p->AddHeader (cModbusRequestPdu);
	if ((m_socket->Send (p)) >= 0)
	{
// to get current time
		gettimeofday(&m_sent_time, NULL);
		++m_sent;
		NS_LOG_INFO ("TraceDelay TX: " << (p->GetSize()-12) << " bytes to " // 8+4 : the size of the seqTs header
		                               << m_peerAddress << " Uid: " << p->GetUid ()
		                               << " Time: " << (Simulator::Now ()).GetSeconds ());
	}
	else
	{
		NS_LOG_INFO ("Error while sending " << m_size << " bytes to "
		                                    << m_peerAddress);
	}
	if (m_modbusSendRequestsIndex < m_modbusRequestsIndex)
	{
		m_sendEvent
			=
				Simulator::Schedule
				        (m_modbusRequestsInterval[m_modbusSendRequestsIndex],
				        &ModbusTcpClient::Send, this);
	}
/*
   if (m_sent < m_count)
   {
   m_sendEvent = Simulator::Schedule (m_interval, &ModbusTcpClient::Send, this);
   }
 */
	if ((m_modbusSendRequestsIndex >= m_modbusRequestsIndex) && (m_sent < m_count))
	{
		m_modbusSendRequestsIndex = 0;
		m_sendEvent
			=
				Simulator::Schedule
				        (m_modbusRequestsInterval[0],
				        &ModbusTcpClient::Send, this);
	}
}
void
ModbusTcpClient::HandleRead (Ptr<Socket> socket)
{
	NS_LOG_INFO ("ModbusTcpClient::HandleRead ()");
	NS_LOG_FUNCTION (this << socket);
	Ptr<Packet> packet;
	Address from;
	while (packet = socket->RecvFrom (from))
	{
		if (InetSocketAddress::IsMatchingType (from))
		{
// to get current time
			gettimeofday(&m_received_time, NULL);
// log round-trip Modbus request/response
//double t1 = m_sent_time.tv_sec + (m_sent_time.tv_usec/1000000.0);
//double t2 = m_received_time.tv_sec + (m_received_time.tv_usec/1000000.0);
//NS_LOG_INFO ("Round-trip Modbus request/response (ms): " << (t2 - t1)*1000);
//std::cout << "Round-trip Modbus request/response (ms): " << (t2 - t1)*1000 << std::endl;
			NS_LOG_INFO ("Received " << packet->GetSize () << " bytes from " <<
			             InetSocketAddress::ConvertFrom (from).GetIpv4 ());
// read the first byte to see it is an exception or successful
			ModbusPdu cModbusResponsePdu;
			packet->RemoveHeader (cModbusResponsePdu);
			uint8_t *pdu = cModbusResponsePdu.GetPdu ();

			if (0x80 <= pdu[0])
			{
				NS_LOG_INFO ("MODBUS response error - function code: 0x" << std::hex << (pdu[0] - 0x80)
				                                                         << " exception code: 0x" << std::hex << (pdu[1] + 0));
				switch (pdu[1])
				{
				case 0x01:
				{
					NS_LOG_INFO ("Invalid funcion or sub-function code");
					break;
				}
				case 0x02:
				{
					NS_LOG_INFO ("Invalid data address");
					break;
				}
				case 0x03:
				{
					NS_LOG_INFO ("Invalid data value");
					break;
				}
				case 0x04:
				case 0x05:
				case 0x06:
				{
					NS_LOG_INFO ("MODBUS function execution failed");
					break;
				}
				default:
				{
					NS_LOG_INFO ("Unknown exception code!");
					break;
				}
				}
				for (uint32_t i = 0; i < 5; ++i)
				{
					NS_LOG_INFO ("pdu[" << std::dec << i << "]: 0x" << std::hex << (pdu[i] + 0));
				}
			}
			else
			{
				NS_LOG_INFO ("Success - function code: 0x" << std::hex << (pdu[0] + 0));
				for (uint32_t i = 0; i < 10; ++i)
				{
					NS_LOG_INFO ("pdu[" << std::dec << i << "]: 0x" << std::hex << (pdu[i] + 0));
				}
			}
			NS_LOG_INFO (std::dec);
		}
	}
}
/*
   void
   ModbusTcpClient::HandlePeerClose (Ptr<Socket> socket)
   {
   NS_LOG_INFO ("ModbusTcpClient, peerClose");
   }
   void
   ModbusTcpClient::HandlePeerError (Ptr<Socket> socket)
   {
   NS_LOG_INFO ("ModbusTcpClient, peerError");
   }
 */
void
ModbusTcpClient::HandleAccept (Ptr<Socket> s, const Address& from)
{
	NS_LOG_FUNCTION (this << s << from);
	s->SetRecvCallback (MakeCallback (&ModbusTcpClient::HandleRead, this));
//m_socketList.push_back (s);
}
} // Namespace ns3