/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#ifndef MODBUS_UDP_CLIENT_H
#define MODBUS_UDP_CLIENT_H

#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/ipv4-address.h"
#include "ns3/traced-callback.h"
#include "sys/time.h"
const uint32_t MAX_NUM_MODBUS_REQUEST = 10; // maximum number of MODBUS request to store

namespace ns3 {

class Socket;
class Packet;

/**
 * \ingroup modbusudpclientserver
 * \class ModbusUdpClient
 * \brief A MODBUS UDP client. Sends MODBUS UDP requests.
 *
 */
class ModbusUdpClient : public Application
{
public:
static TypeId GetTypeId (void);
ModbusUdpClient ();
virtual ~ModbusUdpClient ();
/**
 * \brief set the remote address and port
 * \param ip remote IP address
 * \param port remote port
 */
void SetRemote (Ipv4Address ip, uint16_t port);
/**
 * Set a MODBUS UDP request.
 */
void SetRequest (Time requestInterval, uint8_t *pdu);
/**
 * Set number of iteration for sending the request(s).
 */
void SetCount (uint32_t count);
protected:
virtual void DoDispose (void);
private:
virtual void StartApplication (void);
virtual void StopApplication (void);
void ScheduleTransmit (Time dt);
void Send (void);
void HandleRead (Ptr<Socket> socket);
uint32_t m_count;
Time m_interval;
uint32_t m_size;
uint32_t m_dataSize;
uint8_t *m_data;
uint8_t **m_modbusRequests;
Time *m_modbusRequestsInterval;
uint32_t m_modbusRequestsIndex;
uint32_t m_modbusSendRequestsIndex;
uint32_t m_sent;
Ptr<Socket> m_socket;
Ipv4Address m_peerAddress;
uint16_t m_peerPort;
EventId m_sendEvent;
/// Callbacks for tracing the packet Tx events
TracedCallback<Ptr<const Packet> > m_txTrace;
struct timeval m_sent_time, m_received_time;
};
} // namespace ns3
#endif /* MODBUS_UDP_CLIENT_H */