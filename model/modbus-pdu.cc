/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/header.h"
#include "ns3/simulator.h"
#include "modbus-pdu.h"
NS_LOG_COMPONENT_DEFINE ("ModbusPdu");
namespace ns3 {
NS_OBJECT_ENSURE_REGISTERED (ModbusPdu);
ModbusPdu::ModbusPdu ()
{
	NS_LOG_FUNCTION_NOARGS ();
	m_pdu = new uint8_t [PDU_SIZE];
}
ModbusPdu::~ModbusPdu ()
{
	NS_LOG_FUNCTION_NOARGS ();
	delete [] m_pdu;
	m_pdu = 0;
}
uint8_t*
ModbusPdu::GetPdu (void) const
{
	NS_LOG_FUNCTION_NOARGS ();
	return m_pdu;
}
TypeId
ModbusPdu::GetTypeId (void)
{
	static TypeId tid = TypeId ("ns3::ModbusPdu")
	                    .SetParent<Header> ()
	                    .AddConstructor<ModbusPdu> ()
	;
	return tid;
}
TypeId
ModbusPdu::GetInstanceTypeId (void) const
{
	return GetTypeId ();
}
void
ModbusPdu::Print (std::ostream &os) const
{
	for (uint32_t i = 0; i < PDU_SIZE; ++i)
	{
		os << m_pdu[i] << " ";
	}
	os << std::endl;
}
uint32_t
ModbusPdu::GetSerializedSize (void) const
{
	NS_LOG_FUNCTION_NOARGS ();
	return PDU_SIZE;
}
void
ModbusPdu::Serialize (Buffer::Iterator start) const
{
	NS_LOG_FUNCTION_NOARGS ();
	Buffer::Iterator cIterator = start;
	for (uint32_t i = 0; i < PDU_SIZE; ++i)
	{
		cIterator.WriteU8 (m_pdu[i]);
	}
}
uint32_t
ModbusPdu::Deserialize (Buffer::Iterator start)
{
	NS_LOG_FUNCTION_NOARGS ();
	Buffer::Iterator cIterator = start;
	for (uint32_t i = 0; i < PDU_SIZE; ++i)
	{
		m_pdu[i] = cIterator.ReadU8 ();
	}
	return GetSerializedSize ();
}
} // namespace ns3