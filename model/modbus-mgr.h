/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#ifndef MODBUS_MGR_H
#define MODBUS_MGR_H
#include <stdint.h>
#include <string>
#include "ns3/modbus-pdu.h"
const uint32_t MAX_MODBUS_PDU = 253; // Protocol Data Unit
const uint32_t MAX_COILS_NUM = 65536;
const uint32_t MAX_INPUTS_NUM = 65536;
const uint32_t MAX_HOLDING_REGISTERS = 65536;
const uint32_t MAX_INPUT_REGISTERS = 65536;
const uint32_t MAX_EVENTS = 64;
const uint32_t MAX_FILE_NUMBER = 10; // although can be as high as 0xFFFF,
// but some legacy equipment may be compromised
const uint32_t MAX_FIFO_QUEUE_SIZE = 65536;
const uint32_t MAX_FIFO_COUNT = 31;
const uint8_t READ_COILS = 0x01;
const uint8_t READ_DISCRETE_INPUTS = 0x02;
const uint8_t READ_HOLDING_REGISTERS = 0x03;
const uint8_t READ_INPUT_REGISTERS = 0x04;
const uint8_t WRITE_SINGLE_COIL = 0x05;
const uint8_t WRITE_SINGLE_REGISTER = 0x06;
const uint8_t READ_EXCEPTION_STATUS = 0x07;
const uint8_t DIAGNOSTICS = 0x08;
const uint8_t GET_COMM_EVENT_COUNTER = 0x0B;
const uint8_t GET_COMM_EVENT_LOG = 0x0C;
const uint8_t WRITE_MULTIPLE_COILS = 0x0F;
const uint8_t WRITE_MULTIPLE_REGISTERS = 0x10;
const uint8_t REPORT_SLAVE_ID = 0x11;
const uint8_t READ_FILE_RECORD = 0x14;
const uint8_t WRITE_FILE_RECORD = 0x15;
const uint8_t MASK_WRITE_REGISTER = 0x16;
const uint8_t READ_WRITE_MULTIPLE_REGISTERS = 0x17;
const uint8_t READ_FIFO_QUEUE = 0x18;
const uint8_t ENCAPSULATED_INTERFACE_TRANSPORT = 0x2B;
//
// Diagnostics sub-function codes
//
const uint16_t RETURN_QUERY_DATA = 0x0000;
const uint16_t RESTART_COMMUNICATION_OPTION = 0x0001;
const uint16_t RETURN_DIAGNOSTICS_REGISTER = 0x0002;
const uint16_t CHANGE_ASCII_INPUT_DELIMITER = 0x0003;
const uint16_t FORCE_LISTEN_ONLY = 0x0004;
// 0X05 to 0X09 RESERVED
const uint16_t CLEAR_COUNTERS_AND_DIAGNOSTICS_REGISTER = 0x000A;
const uint16_t RETURN_BUS_MESSAGE_COUNT = 0x000B;
const uint16_t RETURN_BUS_COMMUNICATION_ERROR_COUNT = 0x000C;
const uint16_t RETURN_BUS_EXCEPTION_ERROR_COUNT = 0x000D;
const uint16_t RETURN_SLAVE_MESSAGE_COUNT = 0x000E;
const uint16_t RETURN_SLAVE_NO_RESPONSE_COUNT = 0x000F;
const uint16_t RETURN_SLAVE_NAK_COUNT = 0x0010;
const uint16_t RETURN_SLAVE_BUSY_COUNT = 0x0011;
const uint16_t RETURN_BUS_CHARACTER_OVERRUN_COUNT = 0x0012;
// 0X13 RESERVED
const uint16_t CLEAR_OVERRUN_COUNTER_AND_FLAG = 0x0014;
// 0X15 to 0XFFFF RESERVED
//
// Events code
//
// Remote device MODBUS receive event
const uint8_t REMOTE_DEVICE_MODBUS_RECEIVE_EVENT = 0x80; // bit 7=1
const uint8_t COMMUNICATION_ERROR = 0x02;
const uint8_t CHARACTER_OVERRUN = 0x10;
const uint8_t CURRENTLY_IN_LISTEN_MODE_ONLY = 0x20;
const uint8_t BROADCAST_RECEIVED = 0x40;
// Remote device MODBUS send event
const uint8_t REMOTE_DEVICE_MODBUS_SEND_EVENT = 0x40; // bit 7=0, bit 6=1
const uint8_t READ_EXCEPTION_SENT = 0x01; // exception codes 1-3
const uint8_t SLAVE_ABORT_EXCEPTION_SENT = 0x02; // exception code 4
const uint8_t SLAVE_BUSY_EXCEPTION_SENT = 0x04; // exception code 5-6
const uint8_t SLAVE_PROGRAM_NAK_EXCEPTION_SENT = 0x08; // exception code 7
const uint8_t WRITE_TIMEOUT_ERROR_OCCURRED = 0x10;
const uint8_t CURRENTLY_IN_LISTEN_MODE = 0x20;
// Remote device entered listen only mode
const uint8_t REMOTE_DEVICE_ENTERED_LISTEM_ONLY_MODE = 0x04;
// Remote device initiated communication restart
const uint8_t REMOTE_DEVICE_INITIATED_COMMUNICATION_RESTART = 0x00;
//
// Encapsulated interface transport codes (MEI type)
//
const uint8_t CANOPEN_GENERAL_REFERENCE_REQUEST_AND_RESPONSE_PDU = 0x0D;
const uint8_t READ_DEVICE_IDENTIFICATION = 0x0E;

//
// Read device ID codes
//
const uint8_t READ_BASIC_DEVICE_ID = 0x01;
const uint8_t READ_REGULAR_DEVICE_ID = 0x02;
const uint8_t READ_EXTENDED_DEVICE_ID = 0x03;
const uint8_t READ_SPECIFIC_DEVICE_ID = 0x04;
//
// Basic object IDs
//
const uint8_t VENDOR_NAME_ID = 0x00;
const uint8_t PRODUCT_CODE_ID = 0x01;
const uint8_t MAJOR_MINOR_REVISION_ID = 0x02;
const std::string VENDOR_NAME = "School of Engineering Science - Simon Fraser University"; // Object ID : 0x00
const std::string PRODUCT_CODE = "Product Code XX"; // Object ID: 0x01
const std::string MAJOR_MINOR_REVISION = "V1.0"; // Object ID: 0x02
//
// Conformity level
//
const uint8_t CL_BASIC_ID_STREAM_ACCESS_ONLY = 0x01;
const uint8_t CL_REGULAR_ID_STREAM_ACCESS_ONLY = 0x02;
const uint8_t CL_EXTENDED_ID_STREAM_ACCESS_ONLY = 0x03;
const uint8_t CL_BASIC_ID_STREAM_ACCESS_AND_INDIVIDUAL_ACCESS = 0x81;
const uint8_t CL_REGULAR_ID_STREAM_ACCESS_AND_INDIVIDUAL_ACCESS = 0x82;
const uint8_t CL_EXTENDED_ID_STREAM_ACCESS_AND_INDIVIDUAL_ACCESS = 0x83;

namespace ns3 {
/**
 * \ingroup modbus
 * \class ModbusMgr
 * \brief Manager for MODBUS application
 */
class ModbusMgr
{
public:
ModbusMgr ();
virtual ~ModbusMgr ();
uint8_t* DoMain (uint8_t * pdu);
private:
void PushEventLog (uint8_t event);
uint8_t* ReadCoils (uint16_t startAdr, uint16_t quantityOfCoils);
uint8_t* ReadDiscreteInputs (uint16_t startAdr, uint16_t quantityOfInputs);
uint8_t* ReadHoldingRegisters (uint16_t startAdr, uint16_t quantityOfRegisters);
uint8_t* ReadInputRegisters (uint16_t startAdr, uint16_t quantityOfRegisters);
uint8_t* WriteSingleCoil (uint16_t outputAdr, uint16_t outputValue);
uint8_t* WriteSingleRegister (uint16_t registerAdr, uint16_t registerValue);
uint8_t* ReadExceptionStatus ();
uint8_t* Diagnostics (uint16_t subfunctionCode, uint8_t * pdu);
uint8_t* GetCommEventCounter ();
uint8_t* GetCommEventLog ();
uint8_t* WriteMultipleCoils (uint16_t startAdr, uint16_t quantityOfOutputs,
                             uint8_t byteCount, uint8_t * pdu);
uint8_t* WriteMultipleRegisters (uint16_t startAdr, uint16_t quantityOfRegisters,
                                 uint8_t byteCount, uint8_t * pdu);
uint8_t* ReportSlaveID ();
uint8_t* ReadFileRecord (uint8_t byteCount, uint8_t * pdu);
uint8_t* WriteFileRecord (uint8_t requestDataLength, uint8_t * pdu);
uint8_t* MaskWriteRegister (uint16_t referenceAdr, uint16_t and_mask, uint16_t or_mask);
uint8_t* ReadWriteMultipleRegisters (uint16_t readStartAdr, uint16_t quantityToRead,
                                     uint16_t writeStartAdr, uint16_t quantityToWrite, uint8_t writeByteCount,
                                     uint8_t * pdu);
uint8_t* ReadFifoQueue (uint16_t fifoPointerAdr);
uint8_t* encapsulatedInterfaceTransport (uint8_t meiType, uint8_t * pdu);
uint8_t *m_rspPdu;
uint8_t *m_coilsStatus;
uint8_t *m_inputsStatus;
uint16_t *m_holdingRegisters;
uint16_t *m_inputRegisters;
uint8_t m_exceptionStatusRegister;
bool m_listenOnlyMode;
std::string m_communicationEventLog;
uint16_t m_diagnosticRegister;
std::string m_endOfMessageDelimiter;
uint16_t m_busMessageCounter;
uint16_t m_busCommunicationErrorCounter;
uint16_t m_busExceptionErrorCounter;
uint16_t m_slaveMessageCounter;
uint16_t m_slaveNoResponseCounter;
uint16_t m_slaveNAKCounter;
uint16_t m_slaveBusyCounter;
uint16_t m_busCharacterOverrunCounter;
uint16_t m_commEventCounter;
uint16_t m_EventCounter; // to count the events upto MAX_EVENTS
uint8_t *m_events;
uint16_t **m_fileRecord;
uint16_t *m_fifo;
uint16_t m_fifoCount;
};
} // namespace ns3
#endif /* MODBUS_MGR_H */