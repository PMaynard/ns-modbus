## -*- Mode: python; py-indent-offset: 4; indent-tabs-mode: nil; coding: utf-8; -*-

# def options(opt):
#     pass

# def configure(conf):
#     conf.check_nonfatal(header_name='stdint.h', define_name='HAVE_STDINT_H')

def build(bld):
	module = bld.create_ns3_module('ns-modbus', ['internet', 'config-store', 'applications'])
	# module = bld.create_ns3_module('ns-modbus', ['internet', 'config-store', 'tools'])
	module.module = 'ns-modbus'
	module.source = [
		'model/modbus-udp-client.cc',
		'model/modbus-udp-server.cc',
		'model/modbus-tcp-client.cc',
		'model/modbus-tcp-server.cc',
		'model/modbus-pdu.cc',
		'model/modbus-mgr.cc',
		'helper/modbus-udp-helper.cc',
		'helper/modbus-tcp-helper.cc',
		]

	applications_test = bld.create_ns3_module_test_library('ns-modbus')
	applications_test.source = [
		'test/udp-client-server-test.cc',
		]
	
	headers =  bld(features='ns3header')
	headers.module = 'ns-modbus'
	headers.source = [
		'model/modbus-udp-client.h',
		'model/modbus-udp-server.h',
		'model/modbus-tcp-client.h',
		'model/modbus-tcp-server.h',
		'model/modbus-pdu.h',
		'model/modbus-mgr.h',
		'helper/modbus-udp-helper.h',
		'helper/modbus-tcp-helper.h',
		]

	if bld.env.ENABLE_EXAMPLES:
		bld.recurse('examples')
		
	# bld.ns3_python_bindings()