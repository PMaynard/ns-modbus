/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#ifndef MODBUS_TCP_HELPER_H
#define MODBUS_TCP_HELPER_H
#include <stdint.h>
#include "ns3/application-container.h"
#include "ns3/node-container.h"
#include "ns3/object-factory.h"
#include "ns3/ipv4-address.h"
#include "ns3/modbus-tcp-server.h"
#include "ns3/modbus-tcp-client.h"
namespace ns3 {
/**
 * \brief Create a server application which waits for input MODBUS TCP request
 *
   and generates appropriate response according to the function code.
 *
   It also uses the information carried into their payload of the packet
 *
   to compute delay and to determine if some packets are lost.
 */
class ModbusTcpServerHelper
{
public:
/**
 * Create ModbusTcpServerHelper which will make life easier for people trying
 * to set up simulations with MODBUS TCP application.
 *
 */
ModbusTcpServerHelper ();
/**
 * Record an attribute to be set in each Application after it is created.
 *
 * \param name the name of the attribute to set
 * \param value the value of the attribute to set
 */
void SetAttribute (std::string name, const AttributeValue &value);
/**
 * Create a MODBUS TCP server application on the specified node. The Node
 * is provided as a Ptr<Node>.
 *
 * \param node The Ptr<Node> on which to create the ModbusTcpClientApplication.
 *
 * \returns An ApplicationContainer that holds a Ptr<Application> to the application created
 */
ApplicationContainer Install (Ptr<Node> node) const;
/**
 * Create a MODBUS TCP server application on the specified node. The Node
 * is provided as a string name of a Node that has been previously
 * associated using the Object Name Service.
 *
 * \param nodeName The name of the node on which to create the ModbusTcpClientApplication
 *
 * \returns An ApplicationContainer that holds a Ptr<Application> to the
 *
   application created
 */
ApplicationContainer Install (std::string nodeName) const;
/**
 * Create one MODBUS TCP server application on each of the Nodes in the
 * NodeContainer.
 *
 * \param c The nodes on which to create the Applications. The nodes
 *
   are specified by a NodeContainer.
 * \returns The applications created, one Application per Node in the
 *
   NodeContainer.
 */
ApplicationContainer Install (NodeContainer c) const;
Ptr<ModbusTcpServer> GetServer (void);
private:
Ptr<Application> InstallPriv (Ptr<Node> node) const;
ObjectFactory m_factory;
Ptr<ModbusTcpServer> m_server;
};
/**
 * \brief Create a client application which sends MODBUS TCP requests.
 * The packets carrying a 32bit sequence number and a 64 bit time stamp.
 *
 */
class ModbusTcpClientHelper
{
public:
/**
 * Create ModbusTcpClientHelper which will make life easier for people trying
 * to set up simulations with udp-client-server.
 *
 * \param ip The IP address of the remote MODBUS TCP server
 */
ModbusTcpClientHelper (Ipv4Address ip);
/**
 * Record an attribute to be set in each Application after it is created.
 *
 * \param name the name of the attribute to set
 * \param value the value of the attribute to set
 */
void SetAttribute (std::string name, const AttributeValue &value);
/**
 * \param c the nodes
 *
 * Create one MODBUS TCP client application on each of the input nodes
 *
 * \returns the applications created, one application per input node.
 */
ApplicationContainer Install (NodeContainer c);
Ptr<ModbusTcpClient> GetServer (void);
void SetRequest (Ptr<Application> app, Time requestInterval, uint8_t *pdu);
void SetCount (Ptr<Application> app, uint32_t count);
private:
ObjectFactory m_factory;
Ptr<ModbusTcpClient> m_cModbusTcpClient;
};
} // namespace ns3
#endif /* MODBUS_TCP_HELPER_H */