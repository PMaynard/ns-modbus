/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#include "modbus-udp-helper.h"
//#include "udp-client-server-helper.h"
#include "ns3/udp-trace-client.h"
#include "ns3/uinteger.h"
#include "ns3/string.h"
#include "ns3/names.h"
#include <iostream>
namespace ns3 {
ModbusUdpServerHelper::ModbusUdpServerHelper ()
{
//std::cout << "ModbusUdpServerHelper - Constructor" << std::endl;
	uint16_t port = 502; // standard MODBUS UDP port number
	m_factory.SetTypeId (ModbusUdpServer::GetTypeId ());
	SetAttribute ("Port", UintegerValue (port));
}
void
ModbusUdpServerHelper::SetAttribute (std::string name, const AttributeValue &value)
{
	m_factory.Set (name, value);
}
ApplicationContainer
ModbusUdpServerHelper::Install (Ptr<Node> node) const
{
//std::cout << "ModbusUdpServerHelper - Install (Ptr<Node> node)" << std::endl;
	return ApplicationContainer (InstallPriv (node));
}
ApplicationContainer
ModbusUdpServerHelper::Install (std::string nodeName) const
{
//std::cout << "ModbusUdpServerHelper - Install (std::string nodeName)" << std::endl;
	Ptr<Node> node = Names::Find<Node> (nodeName);
	return ApplicationContainer (InstallPriv (node));
}
ApplicationContainer
ModbusUdpServerHelper::Install (NodeContainer c) const
{
//std::cout << "ModbusUdpServerHelper - Install (NodeContainer c)" << std::endl;
	ApplicationContainer apps;
	for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
	{
		apps.Add (InstallPriv (*i));
	}
	return apps;
}
Ptr<Application>
ModbusUdpServerHelper::InstallPriv (Ptr<Node> node) const
{
//std::cout << "ModbusUdpServerHelper - InstallPriv (Ptr<Node> node)" << std::endl;
	Ptr<Application> app = m_factory.Create<ModbusUdpServer> ();
	node->AddApplication (app);
	return app;
}
Ptr<ModbusUdpServer>
ModbusUdpServerHelper::GetServer (void)
{
	return m_server;
}
ModbusUdpClientHelper::ModbusUdpClientHelper (Ipv4Address address)
{
//std::cout << "ModbusUdpClientHelper - Constructor" << std::endl;
	uint16_t port = 502;
	m_factory.SetTypeId (ModbusUdpClient::GetTypeId ());
	SetAttribute ("RemoteAddress", Ipv4AddressValue (address));
	SetAttribute ("RemotePort", UintegerValue (port));
}
void
ModbusUdpClientHelper::SetAttribute (std::string name, const AttributeValue &value)
{
	m_factory.Set (name, value);
}
ApplicationContainer
ModbusUdpClientHelper::Install (NodeContainer c)
{
//std::cout << "ModbusUdpClientHelper - Install" << std::endl;
	ApplicationContainer apps;
	for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
	{
		Ptr<Node> node = *i;
		Ptr<ModbusUdpClient> client = m_factory.Create<ModbusUdpClient> ();
		node->AddApplication (client);
		apps.Add (client);
	}
	return apps;
}
void
ModbusUdpClientHelper::SetRequest (Ptr<Application> app, Time requestInterval, uint8_t *pdu)
{
//std::cout << "ModbusUdpClientHelper - SetRequest" << std::endl;
	app->GetObject<ModbusUdpClient>()->SetRequest (requestInterval, pdu);
}
void
ModbusUdpClientHelper::SetCount (Ptr<Application> app, uint32_t count)
{
//std::cout << "ModbusUdpClientHelper - SetCount" << std::endl;
	app->GetObject<ModbusUdpClient>()->SetCount (count);
}
ModbusUdpTraceClientHelper::ModbusUdpTraceClientHelper ()
{
}
ModbusUdpTraceClientHelper::ModbusUdpTraceClientHelper (Ipv4Address address, std::string
                                                        filename)
{
	uint16_t port = 502;
	m_factory.SetTypeId (UdpTraceClient::GetTypeId ());
	SetAttribute ("RemoteAddress", Ipv4AddressValue (address));
	SetAttribute ("RemotePort", UintegerValue (port));
	SetAttribute ("TraceFilename", StringValue (filename));
}
void
ModbusUdpTraceClientHelper::SetAttribute (std::string name, const AttributeValue &value)
{
	m_factory.Set (name, value);
}
ApplicationContainer
ModbusUdpTraceClientHelper::Install (NodeContainer c)
{
	ApplicationContainer apps;
	for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
	{
		Ptr<Node> node = *i;
		Ptr<UdpTraceClient> client = m_factory.Create<UdpTraceClient> ();
		node->AddApplication (client);
		apps.Add (client);
	}
	return apps;
}
} // namespace ns3