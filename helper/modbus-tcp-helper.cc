/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Simon Fraser University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Author: Reza Sahraei <mrs16@sfu.ca>
 */
#include "modbus-tcp-helper.h"
//#include "ns3/tcp-trace-client.h"
#include "ns3/uinteger.h"
#include "ns3/string.h"
#include "ns3/names.h"
#include <iostream>
namespace ns3 {
ModbusTcpServerHelper::ModbusTcpServerHelper ()
{
//std::cout << "ModbusTcpServerHelper - Constructor" << std::endl;
	uint16_t port = 502; // standard MODBUS TCP port number
	m_factory.SetTypeId (ModbusTcpServer::GetTypeId ());
	SetAttribute ("Port", UintegerValue (port));
}
void
ModbusTcpServerHelper::SetAttribute (std::string name, const AttributeValue &value)
{
	m_factory.Set (name, value);
}
ApplicationContainer
ModbusTcpServerHelper::Install (Ptr<Node> node) const
{
//std::cout << "ModbusTcpServerHelper - Install (Ptr<Node> node)" << std::endl;
	return ApplicationContainer (InstallPriv (node));
}
ApplicationContainer
ModbusTcpServerHelper::Install (std::string nodeName) const
{
//std::cout << "ModbusTcpServerHelper - Install (std::string nodeName)" << std::endl;
	Ptr<Node> node = Names::Find<Node> (nodeName);
	return ApplicationContainer (InstallPriv (node));
}
ApplicationContainer
ModbusTcpServerHelper::Install (NodeContainer c) const
{
//std::cout << "ModbusTcpServerHelper - Install (NodeContainer c)" << std::endl;
	ApplicationContainer apps;
	for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
	{
		apps.Add (InstallPriv (*i));
	}
	return apps;
}
Ptr<Application>
ModbusTcpServerHelper::InstallPriv (Ptr<Node> node) const
{
//std::cout << "ModbusTcpServerHelper - InstallPriv (Ptr<Node> node)" << std::endl;
	Ptr<Application> app = m_factory.Create<ModbusTcpServer> ();
	node->AddApplication (app);
	return app;
}
Ptr<ModbusTcpServer>
ModbusTcpServerHelper::GetServer (void)
{
	return m_server;
}
ModbusTcpClientHelper::ModbusTcpClientHelper (Ipv4Address address)
{
//std::cout << "ModbusTcpClientHelper - Constructor" << std::endl;
	uint16_t port = 502;
	m_factory.SetTypeId (ModbusTcpClient::GetTypeId ());
	SetAttribute ("RemoteAddress", Ipv4AddressValue (address));
	SetAttribute ("RemotePort", UintegerValue (port));
}
void
ModbusTcpClientHelper::SetAttribute (std::string name, const AttributeValue &value)
{
	m_factory.Set (name, value);
}
ApplicationContainer
ModbusTcpClientHelper::Install (NodeContainer c)
{
//std::cout << "ModbusTcpClientHelper - Install" << std::endl;
	ApplicationContainer apps;
	for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
	{
		Ptr<Node> node = *i;
		Ptr<ModbusTcpClient> client = m_factory.Create<ModbusTcpClient> ();
		node->AddApplication (client);
		apps.Add (client);
	}
	return apps;
}
void
ModbusTcpClientHelper::SetRequest (Ptr<Application> app, Time requestInterval, uint8_t *pdu)
{
//std::cout << "ModbusTcpClientHelper - SetRequest" << std::endl;
	app->GetObject<ModbusTcpClient>()->SetRequest (requestInterval, pdu);
}
void
ModbusTcpClientHelper::SetCount (Ptr<Application> app, uint32_t count)
{
//std::cout << "ModbusTcpClientHelper - SetCount" << std::endl;
	app->GetObject<ModbusTcpClient>()->SetCount (count);
}
} // namespace ns3